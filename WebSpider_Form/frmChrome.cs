﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;


namespace WebSpider
{
    public partial class frmChrome : Form
    {
        static ChromiumWebBrowser browser;
        public void InitBrowser()
        {


            Cef.Initialize(new CefSettings());
            //browser = new ChromiumWebBrowser("www.google.com");
            browser = new ChromiumWebBrowser(null);


            this.Controls.Add(browser);
            browser.Dock = DockStyle.Fill;

            //browser.Load("https://www.pagesjaunes.fr/pagesblanchesou=All%c3%a9e+Berlioz%2c+Saint-Denis-l%c3%a8s-Bourg+(01000)&proximite=0");
            browser.Load("https://www.pagesjaunes.fr/recherche/Boulevard+d+Alsac%c3%a9%2c+Lille+(59000)/-");

            browser.LoadingStateChanged += OnLoadingStateChanged;

        }
        
        //Wait for the page to finish loading (all resources will have been loaded, rendering is likely still happening)
        //This callback will be executed twice once when loading is initiated either programmatically or by user action, 
        //and once when loading is terminated due to completion, cancellation of failure. 
        //This method will be called on the CEF UI thread. 
        //Blocking this thread will likely cause your UI to become unresponsive and/or hang.        
        private void OnLoadingStateChanged(object sender, LoadingStateChangedEventArgs args)
        {
            if (!args.IsLoading)
            {
                // Page has finished loading, do whatever you want here               
                browser.LoadingStateChanged -= OnLoadingStateChanged;

                Task test = DoWork();
                //var html = browser.GetSourceAsync();

                //string test = html.Result;

                //int y = 5;

            }
        }

        public async Task DoWork()
        {
            try
            {
                string html = await browser.GetSourceAsync();

                if (html != null)
                {
                    int i = 0;
                    //browser.LoadingStateChanged -= OnLoadingStateChanged;

                }

            }
            catch (Exception)
            {

                throw;
            }

        }

        public frmChrome()
        {
            InitializeComponent();
            InitBrowser();
        }
    }
}
