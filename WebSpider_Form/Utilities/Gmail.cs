﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;
using Tools;

namespace WebSpider.Utilities
{
    class Gmail
    {

        public void SendEmail(string pMessage)
        {
            try
            {
                MailAddress fromAddress = new MailAddress("geowebspider@gmail.com", "GEO WEB SPIDER");
                //var toAddress = new MailAddress("bansee.vimal@gmail.com" , "Vimal BANSEE");
                string toAddress = "bansee.vimal@gmail.com, informatique@oceancallcentre.com";
                string fromPassword = "oceancall";
                string subject = "WEBSPIDER ALERTE";
                string body = pMessage;

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                    Timeout = 20000
                };

                MailMessage message = new MailMessage();
                message.From = fromAddress;
                message.Subject = subject;
                message.Body = body;

                if (Global.EMAIL_TO.Trim() != string.Empty)
                    toAddress = toAddress + "," + Global.EMAIL_TO;

                message.To.Add(toAddress);
                smtp.Send(message);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public void SendEmailTest(string pMessage)
        {
            try
            {
                MailAddress fromAddress = new MailAddress("geowebspider@gmail.com", "GEO WEB SPIDER");
                //var toAddress = new MailAddress("bansee.vimal@gmail.com" , "Vimal BANSEE");
                string toAddress = "bansee.vimal@gmail.com";
                string subject = "WEBSPIDER ALERTE";
                string body = pMessage;

                var smtp = new SmtpClient
                {
                    Host = "193.70.3.13",
                    Port = 25
                };

                MailMessage message = new MailMessage();
                message.From = fromAddress;
                message.Subject = subject;
                message.Body = body;

                message.To.Add(toAddress);
                smtp.Send(message);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
    }
}
