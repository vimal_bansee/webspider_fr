﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Tools;

namespace WebSpider
{
    public partial class frmThreadControl : Form
    {
        private frmExtractionWeb _frmExtractionWeb;

        private Thread _myThread;

        private int _communeIndex = -1;

        public frmThreadControl()
        {
            InitializeComponent();

            myTimer.Interval = Global.RESTART_INTERVAL * 1000;
            myTimer.Enabled = true;
            myTimer.Start();

            StartThread();
        }

        private void myTimer_Tick(object sender, EventArgs e)
        {            
            while (_myThread.IsAlive)
            {
                _myThread.Join(5000);
                _myThread.Interrupt();
                _myThread.Abort();
            }

            _communeIndex = _frmExtractionWeb.CommuneIndex;

            GC.Collect();
            GC.WaitForPendingFinalizers();

            StartThread();

        }

        public void StartThread()
        {
            try
            {
                _myThread = new Thread(LaunchForm);
                _myThread.ApartmentState = ApartmentState.STA;
                _myThread.Start();
            }
            catch (Exception)
            {

            }
        }

        public void LaunchForm()
        {
            try
            {
                _frmExtractionWeb = new frmExtractionWeb();
                _frmExtractionWeb.CommuneIndex = _communeIndex;
                _frmExtractionWeb.ShowDialog();                
            }
            catch (Exception ex)
            {

            }
        }

    }
}
