namespace WebSpider
{
    partial class frmExtractionData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpRepertoire = new System.Windows.Forms.GroupBox();
            this.picFolder = new System.Windows.Forms.PictureBox();
            this.txtRepertoire = new System.Windows.Forms.TextBox();
            this.btnExtract = new System.Windows.Forms.Button();
            this.lblNomFichier = new System.Windows.Forms.Label();
            this.lblRepertoire = new System.Windows.Forms.Label();
            this.txtNomFichier = new System.Windows.Forms.TextBox();
            this.grpStats = new System.Windows.Forms.GroupBox();
            this.dgInfoCP = new System.Windows.Forms.DataGridView();
            this.statsCP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statsCommune = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statsNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statsSelection = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.grpCritere = new System.Windows.Forms.GroupBox();
            this.lblTypeAnnuaire = new System.Windows.Forms.Label();
            this.rdProfessionnel = new System.Windows.Forms.RadioButton();
            this.rdParticuliers = new System.Windows.Forms.RadioButton();
            this.btnRefreshData = new System.Windows.Forms.Button();
            this.lblDept = new System.Windows.Forms.Label();
            this.dpDept = new System.Windows.Forms.ComboBox();
            this.lblError = new System.Windows.Forms.Label();
            this.grpRepertoire.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFolder)).BeginInit();
            this.grpStats.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInfoCP)).BeginInit();
            this.grpCritere.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpRepertoire
            // 
            this.grpRepertoire.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grpRepertoire.Controls.Add(this.picFolder);
            this.grpRepertoire.Controls.Add(this.txtRepertoire);
            this.grpRepertoire.Controls.Add(this.btnExtract);
            this.grpRepertoire.Controls.Add(this.lblNomFichier);
            this.grpRepertoire.Controls.Add(this.lblRepertoire);
            this.grpRepertoire.Controls.Add(this.txtNomFichier);
            this.grpRepertoire.Font = new System.Drawing.Font("Corbel", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpRepertoire.Location = new System.Drawing.Point(703, 91);
            this.grpRepertoire.Name = "grpRepertoire";
            this.grpRepertoire.Size = new System.Drawing.Size(369, 272);
            this.grpRepertoire.TabIndex = 44;
            this.grpRepertoire.TabStop = false;
            this.grpRepertoire.Text = "Extraire des donn�es";
            // 
            // picFolder
            // 
            this.picFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picFolder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picFolder.Image = global::WebSpider.Properties.Resources.Directory;
            this.picFolder.Location = new System.Drawing.Point(318, 42);
            this.picFolder.Name = "picFolder";
            this.picFolder.Size = new System.Drawing.Size(40, 40);
            this.picFolder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picFolder.TabIndex = 43;
            this.picFolder.TabStop = false;
            this.picFolder.Click += new System.EventHandler(this.picFolder_Click);
            // 
            // txtRepertoire
            // 
            this.txtRepertoire.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRepertoire.Location = new System.Drawing.Point(19, 53);
            this.txtRepertoire.Name = "txtRepertoire";
            this.txtRepertoire.ReadOnly = true;
            this.txtRepertoire.Size = new System.Drawing.Size(291, 24);
            this.txtRepertoire.TabIndex = 3;
            // 
            // btnExtract
            // 
            this.btnExtract.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExtract.Image = global::WebSpider.Properties.Resources.Extraction;
            this.btnExtract.Location = new System.Drawing.Point(145, 143);
            this.btnExtract.Name = "btnExtract";
            this.btnExtract.Size = new System.Drawing.Size(90, 103);
            this.btnExtract.TabIndex = 5;
            this.btnExtract.UseVisualStyleBackColor = true;
            this.btnExtract.Click += new System.EventHandler(this.btnExtract_Click);
            // 
            // lblNomFichier
            // 
            this.lblNomFichier.AutoSize = true;
            this.lblNomFichier.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblNomFichier.Location = new System.Drawing.Point(17, 84);
            this.lblNomFichier.Name = "lblNomFichier";
            this.lblNomFichier.Size = new System.Drawing.Size(103, 15);
            this.lblNomFichier.TabIndex = 41;
            this.lblNomFichier.Text = "Nom du Fichier:";
            // 
            // lblRepertoire
            // 
            this.lblRepertoire.AutoSize = true;
            this.lblRepertoire.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblRepertoire.Location = new System.Drawing.Point(17, 37);
            this.lblRepertoire.Name = "lblRepertoire";
            this.lblRepertoire.Size = new System.Drawing.Size(77, 15);
            this.lblRepertoire.TabIndex = 39;
            this.lblRepertoire.Text = "R�pertoire:";
            // 
            // txtNomFichier
            // 
            this.txtNomFichier.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNomFichier.Location = new System.Drawing.Point(19, 101);
            this.txtNomFichier.Name = "txtNomFichier";
            this.txtNomFichier.Size = new System.Drawing.Size(291, 24);
            this.txtNomFichier.TabIndex = 4;
            // 
            // grpStats
            // 
            this.grpStats.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpStats.Controls.Add(this.dgInfoCP);
            this.grpStats.Font = new System.Drawing.Font("Corbel", 11.25F, System.Drawing.FontStyle.Bold);
            this.grpStats.Location = new System.Drawing.Point(12, 89);
            this.grpStats.Name = "grpStats";
            this.grpStats.Size = new System.Drawing.Size(667, 400);
            this.grpStats.TabIndex = 45;
            this.grpStats.TabStop = false;
            this.grpStats.Text = "Stats D\'extraction";
            // 
            // dgInfoCP
            // 
            this.dgInfoCP.AllowUserToAddRows = false;
            this.dgInfoCP.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.dgInfoCP.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgInfoCP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgInfoCP.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.statsCP,
            this.statsCommune,
            this.statsNombre,
            this.statsSelection});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgInfoCP.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgInfoCP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgInfoCP.Location = new System.Drawing.Point(3, 22);
            this.dgInfoCP.Name = "dgInfoCP";
            this.dgInfoCP.Size = new System.Drawing.Size(661, 375);
            this.dgInfoCP.TabIndex = 2;
            // 
            // statsCP
            // 
            this.statsCP.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.statsCP.DataPropertyName = "CP";
            this.statsCP.FillWeight = 203.0457F;
            this.statsCP.HeaderText = "CP";
            this.statsCP.Name = "statsCP";
            // 
            // statsCommune
            // 
            this.statsCommune.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.statsCommune.DataPropertyName = "Commune";
            this.statsCommune.FillWeight = 10.15228F;
            this.statsCommune.HeaderText = "Commune";
            this.statsCommune.Name = "statsCommune";
            this.statsCommune.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.statsCommune.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // statsNombre
            // 
            this.statsNombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.statsNombre.DataPropertyName = "Nombre";
            this.statsNombre.FillWeight = 176.6497F;
            this.statsNombre.HeaderText = "Nombre";
            this.statsNombre.MinimumWidth = 100;
            this.statsNombre.Name = "statsNombre";
            // 
            // statsSelection
            // 
            this.statsSelection.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.statsSelection.FillWeight = 10.15228F;
            this.statsSelection.HeaderText = "S�lection";
            this.statsSelection.MinimumWidth = 100;
            this.statsSelection.Name = "statsSelection";
            this.statsSelection.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // grpCritere
            // 
            this.grpCritere.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.grpCritere.Controls.Add(this.lblTypeAnnuaire);
            this.grpCritere.Controls.Add(this.rdProfessionnel);
            this.grpCritere.Controls.Add(this.rdParticuliers);
            this.grpCritere.Controls.Add(this.btnRefreshData);
            this.grpCritere.Controls.Add(this.lblDept);
            this.grpCritere.Controls.Add(this.dpDept);
            this.grpCritere.Font = new System.Drawing.Font("Corbel", 11.25F, System.Drawing.FontStyle.Bold);
            this.grpCritere.Location = new System.Drawing.Point(102, 2);
            this.grpCritere.Name = "grpCritere";
            this.grpCritere.Size = new System.Drawing.Size(518, 83);
            this.grpCritere.TabIndex = 46;
            this.grpCritere.TabStop = false;
            // 
            // lblTypeAnnuaire
            // 
            this.lblTypeAnnuaire.AutoSize = true;
            this.lblTypeAnnuaire.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTypeAnnuaire.Location = new System.Drawing.Point(30, 18);
            this.lblTypeAnnuaire.Name = "lblTypeAnnuaire";
            this.lblTypeAnnuaire.Size = new System.Drawing.Size(111, 15);
            this.lblTypeAnnuaire.TabIndex = 30;
            this.lblTypeAnnuaire.Text = "Type D\'annuaire:";
            // 
            // rdProfessionnel
            // 
            this.rdProfessionnel.AutoSize = true;
            this.rdProfessionnel.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdProfessionnel.Location = new System.Drawing.Point(57, 59);
            this.rdProfessionnel.Name = "rdProfessionnel";
            this.rdProfessionnel.Size = new System.Drawing.Size(110, 19);
            this.rdProfessionnel.TabIndex = 29;
            this.rdProfessionnel.Text = "Professionnels ";
            this.rdProfessionnel.UseVisualStyleBackColor = true;
            // 
            // rdParticuliers
            // 
            this.rdParticuliers.AutoSize = true;
            this.rdParticuliers.Checked = true;
            this.rdParticuliers.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdParticuliers.Location = new System.Drawing.Point(57, 37);
            this.rdParticuliers.Name = "rdParticuliers";
            this.rdParticuliers.Size = new System.Drawing.Size(89, 19);
            this.rdParticuliers.TabIndex = 28;
            this.rdParticuliers.TabStop = true;
            this.rdParticuliers.Text = "Particuliers";
            this.rdParticuliers.UseVisualStyleBackColor = true;
            // 
            // btnRefreshData
            // 
            this.btnRefreshData.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefreshData.Image = global::WebSpider.Properties.Resources.Refresh;
            this.btnRefreshData.Location = new System.Drawing.Point(441, 16);
            this.btnRefreshData.Name = "btnRefreshData";
            this.btnRefreshData.Size = new System.Drawing.Size(60, 60);
            this.btnRefreshData.TabIndex = 27;
            this.btnRefreshData.UseVisualStyleBackColor = true;
            this.btnRefreshData.Click += new System.EventHandler(this.btnRefreshData_Click);
            // 
            // lblDept
            // 
            this.lblDept.AutoSize = true;
            this.lblDept.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDept.Location = new System.Drawing.Point(213, 19);
            this.lblDept.Name = "lblDept";
            this.lblDept.Size = new System.Drawing.Size(92, 15);
            this.lblDept.TabIndex = 26;
            this.lblDept.Text = "D�partement:";
            // 
            // dpDept
            // 
            this.dpDept.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dpDept.FormattingEnabled = true;
            this.dpDept.Location = new System.Drawing.Point(218, 40);
            this.dpDept.Name = "dpDept";
            this.dpDept.Size = new System.Drawing.Size(168, 25);
            this.dpDept.TabIndex = 25;
            // 
            // lblError
            // 
            this.lblError.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(680, 12);
            this.lblError.MaximumSize = new System.Drawing.Size(350, 68);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(105, 17);
            this.lblError.TabIndex = 28;
            this.lblError.Text = "Message Erreur";
            // 
            // frmExtractionData
            // 
            this.ClientSize = new System.Drawing.Size(1084, 492);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.grpCritere);
            this.Controls.Add(this.grpStats);
            this.Controls.Add(this.grpRepertoire);
            this.DoubleBuffered = true;
            this.Name = "frmExtractionData";
            this.Text = "Extraction Data";
            this.Load += new System.EventHandler(this.frmExtractionData_Load);
            this.grpRepertoire.ResumeLayout(false);
            this.grpRepertoire.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFolder)).EndInit();
            this.grpStats.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgInfoCP)).EndInit();
            this.grpCritere.ResumeLayout(false);
            this.grpCritere.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.GroupBox grpRepertoire;
        internal System.Windows.Forms.PictureBox picFolder;
        internal System.Windows.Forms.TextBox txtRepertoire;
        internal System.Windows.Forms.Button btnExtract;
        internal System.Windows.Forms.Label lblNomFichier;
        internal System.Windows.Forms.Label lblRepertoire;
        internal System.Windows.Forms.TextBox txtNomFichier;
        private System.Windows.Forms.GroupBox grpStats;
        internal System.Windows.Forms.DataGridView dgInfoCP;
        private System.Windows.Forms.GroupBox grpCritere;
        private System.Windows.Forms.Label lblDept;
        private System.Windows.Forms.ComboBox dpDept;
        internal System.Windows.Forms.Button btnRefreshData;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Label lblTypeAnnuaire;
        private System.Windows.Forms.RadioButton rdProfessionnel;
        private System.Windows.Forms.RadioButton rdParticuliers;
        private System.Windows.Forms.DataGridViewTextBoxColumn statsCP;
        private System.Windows.Forms.DataGridViewTextBoxColumn statsCommune;
        private System.Windows.Forms.DataGridViewTextBoxColumn statsNombre;
        private System.Windows.Forms.DataGridViewCheckBoxColumn statsSelection;
    }
}