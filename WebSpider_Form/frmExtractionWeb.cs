﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using WebSpider.Utilities;
using System.Web;
using System.Threading;
using System.Data.SqlClient;
using SharpCore.Data;
using WebSpider_DLL;
using WebSpider_DLL.DAL;
using Tools;
using System.Net;
using CefSharp;
using CefSharp.WinForms;
using System.Threading.Tasks;
using System.Timers;

namespace WebSpider
{
    public partial class frmExtractionWeb : DevComponents.DotNetBar.Office2007Form
    {

        private const string PARTICULIER_URL = "https://www.pagesjaunes.fr/pagesblanches/recherche?";  //https://www.pagesjaunes.fr/pagesblanches/recherche?ou=Boulevard%20d%27Alsac%C3%A9%2C%20Lille%20%2859000%29&proximite=0

        private const string PROFESSIONNEL_URL = "http://www.pagesjaunes.fr/annuaire/chercherlespros?";

        private string _computerName = string.Empty;

        private int _compteur = 0;

        private int _pageNo = 0;

        private int _nombreContactExtrait = 0;

        private bool _stop = false;
        public bool Stop
        {
            set
            {
                _stop = value;
            }
            get
            {
                return _stop;
            }

        }

        private bool _canAbort = false;
        public bool CanAbort
        {
            set
            {
                _canAbort = value;
            }
            get
            {
                return _canAbort;
            }
        }

        private bool _isBusy = false;

        private RefAdresse _refAdresseEnCours = null;

        private List<InfoExtrait> _lesInfoExtrait = null;

        HashSet<string> AdresseList = null;

        HashSet<string> CommuneList = null;

        private int _communeIndex = -1;
        public int CommuneIndex
        {
            set
            {
                _communeIndex = value;
            }
            get
            {
                return _communeIndex;
            }
        }

        private string _communeName = null;
        public string CommuneName
        {
            set
            {
                _communeName = value;
            }
            get
            {
                return _communeName;
            }
        }

        private string _communeCP = null;
        public string CommuneCP
        {
            set
            {
                _communeCP = value;
            }
            get
            {
                return _communeCP;
            }
        }

        public frmExtractionWeb()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception)
            {

            }
        }

        private void frmExtractionWeb_Load(object sender, EventArgs e)
        {
            try
            {
                AdresseList = new HashSet<string>();

                InitializeChromium();

                _computerName = System.Net.Dns.GetHostName();
                System.Net.IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(_computerName);
                System.Net.IPAddress[] ipAddress = ipEntry.AddressList;

                if (ipAddress.Length > 1)
                    _computerName = "Machine: " + _computerName + " | IP: " + ipAddress[1].ToString();
                else if (ipAddress.Length >= 1)
                    _computerName = "Machine: " + _computerName + " | IP: " + ipAddress[0].ToString();
                else
                    _computerName = "Machine: " + _computerName;

                string appName = Process.GetCurrentProcess().ProcessName;
                WebBrowserHelper.FixBrowserVersion(appName);

                lblError.Text = string.Empty;
                picError.Visible = false;

                DisplayCommune();

                InitialiseRobot();

                btnPlay.Enabled = true;
                btnStop.Enabled = false;

                //AutoStart Application
                if (CommuneIndex > -1)
                {
                    chkResetEchec.Checked = true;
                    btnPlay_Click(btnPlay, null);
                }

            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                picError.Visible = true;
            }
            finally
            {

            }

        }

        public ChromiumWebBrowser chromeBrowser;

        public void InitializeChromium()
        {
            CefSettings settings = new CefSettings();
            settings.IgnoreCertificateErrors = true;

            // Initialize cef with the provided settings
            Cef.Initialize(settings);

            CefSharp.BrowserSettings browserSetting = new CefSharp.BrowserSettings();
            browserSetting.UniversalAccessFromFileUrls = CefState.Enabled;
            browserSetting.WebSecurity = CefState.Enabled;
            browserSetting.Plugins = CefState.Default;
            browserSetting.DefaultEncoding = "utf-8";

            // Create a browser component
            chromeBrowser = new ChromiumWebBrowser();
            chromeBrowser.BrowserSettings = browserSetting;

            // Add it to the form and fill it to the form window.
            panelChrome.Controls.Add(chromeBrowser);
            chromeBrowser.Dock = DockStyle.Fill;

            //chromeBrowser.LoadingStateChanged += OnLoadingStateChanged;
        }

        System.Timers.Timer timerCheckIdle;

        private void TimerElapsedCheckIdle(object sender, ElapsedEventArgs e)
        {
            Task task = CheckIdleTime();
        }

        private async Task CheckIdleTime()
        {
            DateTime lastTimeUpdate = _refAdresseEnCours.DateModifier;
            DateTime currentDateTime = Tools.DB.GetCurrentDateTime();

            TimeSpan timeSpan = currentDateTime.Subtract(lastTimeUpdate);

            if (timeSpan.Minutes > 20)
            {
                SearchReferenceAddress();

                /*
                string url = chromeBrowser.Address;

                if (url != null)
                {
                    chromeBrowser.LoadingStateChanged += OnLoadingStateChanged;
                    chromeBrowser.Load(url);
                }
                */
            }

        }

        //Wait for the page to finish loading (all resources will have been loaded, rendering is likely still happening)
        //This callback will be executed twice once when loading is initiated either programmatically or by user action, 
        //and once when loading is terminated due to completion, cancellation of failure. 
        //This method will be called on the CEF UI thread. 
        //Blocking this thread will likely cause your UI to become unresponsive and/or hang.        
        private void OnLoadingStateChanged(object sender, LoadingStateChangedEventArgs args)
        {
            if (!args.IsLoading)
            {
                //if (AdresseList.Contains(chromeBrowser.Address) == false)
                //{
                //    AdresseList.Add(chromeBrowser.Address);
                //    Task task = DoWork();
                //}

                chromeBrowser.LoadingStateChanged -= OnLoadingStateChanged;

                Task task = DoWork();
            }
            else
            {
                if (_timer == null)
                {
                    _timer = new System.Timers.Timer(120000); //120 seconds = 2 mins

                    _timer.Elapsed += new ElapsedEventHandler(TimerElapsed);
                    _timer.Start();

                }
                else
                {
                    _timer.Start();
                }

            }
        }

        System.Timers.Timer _timer;
        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            //_timer.Stop();

            string url = chromeBrowser.Address;

            if (url != null)
            {
                chromeBrowser.LoadingStateChanged += OnLoadingStateChanged;
                chromeBrowser.Load(url);
            }
        }

        private async Task DoWork()
        {
            try
            {
                bool canExtract = false;
                Stopwatch stopwatchCompleted = new Stopwatch();
                string html;
                bool isRobot = false;

                stopwatchCompleted.Start();
                while (true)
                {
                    html = await chromeBrowser.GetSourceAsync();

                    try
                    {
                        if (!string.IsNullOrEmpty(html))
                        {
                            //browser.LoadingStateChanged -= OnLoadingStateChanged;

                            if (html.IndexOf("Suivant") != -1)
                            {
                                canExtract = true;
                                break;
                            }
                            else if (html.IndexOf("</footer>") != -1)
                            {
                                canExtract = false;
                                break;
                            }
                            else if (html.IndexOf("<title>You have been blocked</title>") != -1)
                            {
                                isRobot = true;
                                canExtract = false;
                                break;
                            }
                            else if (stopwatchCompleted.ElapsedMilliseconds >= 25000) //15 secs d'attendre pour completer la page
                            {
                                stopwatchCompleted.Stop();
                                canExtract = false;
                                break;
                                //throw new Exception("Pages Jaunes est inaccessible. \nVeuillez contacter votre administrateur systeme.");

                            }
                        }
                    }
                    catch (Exception)
                    {
                        //throw;
                    }

                }
                stopwatchCompleted.Stop();

                if (canExtract)
                {
                    _timer.Stop();

                    if (AdresseList == null)
                        AdresseList = new HashSet<string>();

                    if (AdresseList.Contains(chromeBrowser.Address) == false)
                    {
                        AdresseList.Add(chromeBrowser.Address);

                        StartExtraction(html);

                        ExtractURL(html);
                    }

                }
                else
                {

                    if (isRobot)
                    {
                        isRobot = false;
                        _timer.Stop();
                        throw new Exception("PageJaune a été bloqué, veuillez accéder à l'application pour débloquer l'extraction manuellement en sélectionnant les images correctes pour se authentifier.");
                    }
                    else
                    {
                        UpdateRefAddress("Le contenu de la page ne peut pas être extrait", RefAdresseDAL.ENUM_STATUS.FAILED);
                        SearchReferenceAddress();
                    }
                }

            }
            catch (Exception ex)
            {
                HandleExceptionMessage(new Exception("DoWork: " + ex.Message), true);
            }

        }

        private void WaitExtraction()
        {
            Random random = new Random();
            int minSec = Global.MIN_INTERVAL;
            int maxSec = Global.MAX_INTERVAL;
            int randomMillisecond = random.Next(minSec * 1000, maxSec * 1000);
            //System.Threading.Thread.Sleep(randomMillisecond);

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            while (true)
            {
                if (Stop)
                {
                    break;
                }
                else if (stopwatch.ElapsedMilliseconds >= randomMillisecond)
                {
                    stopwatch.Stop();
                    break;
                }
                else
                {
                    Application.DoEvents();
                }
            }
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = string.Empty;
                picError.Visible = false;

                btnPlay.Enabled = false;
                //btnStop.Enabled = true;
                //Stop = false;
                //_isBusy = true;

                //this.UseWaitCursor = true;

                _nombreContactExtrait = 0;

                if (ccbCP.CheckedItems.Count > 0)
                {
                    timerCheckIdle = new System.Timers.Timer(600000);
                    timerCheckIdle.Elapsed += new ElapsedEventHandler(TimerElapsedCheckIdle);
                    timerCheckIdle.Start();

                    lblError.Invoke(new Action(() => lblError.Text = string.Empty));

                    AutoSelectCommune();
                }
                else
                {
                    lblError.Text = "Veuillez sélectionner le code postal et la commune pour procéder! Merci";
                    picError.Visible = true;
                }

                //btnPlay.Enabled = true;
                //btnStop.Enabled = false;
                //Stop = false;
                //_isBusy = false;

                //Cursor = Cursors.Default;
                //this.UseWaitCursor = false;

                //CanAbort = true;

            }
            catch (Exception)
            {

            }
            finally
            {

            }

        }

        private void btnPlay_MouseHover(object sender, EventArgs e)
        {
            this.UseWaitCursor = false;
            Cursor = Cursors.Hand;
        }

        private void btnPlay_MouseLeave(object sender, EventArgs e)
        {
            if (_isBusy)
                this.UseWaitCursor = true;
            else
                Cursor = Cursors.Default;

        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            //btnPlay.Enabled = true;
            btnStop.Enabled = false;

            Stop = true;
        }

        private void btnStop_MouseHover(object sender, EventArgs e)
        {
            this.UseWaitCursor = false;
            Cursor = Cursors.Hand;
        }

        private void btnStop_MouseLeave(object sender, EventArgs e)
        {
            if (_isBusy)
                this.UseWaitCursor = true;
            else
                Cursor = Cursors.Default;
        }

        private void btnExtract_Click(object sender, EventArgs e)
        {
            frmExtractionData extractionData = new frmExtractionData();
            extractionData.ShowDialog();
        }

        private void btnExtract_MouseHover(object sender, EventArgs e)
        {
            this.UseWaitCursor = false;
            Cursor = Cursors.Hand;
        }

        private void btnExtract_MouseLeave(object sender, EventArgs e)
        {
            if (_isBusy)
                this.UseWaitCursor = true;
            else
                Cursor = Cursors.Default;

        }

        public void InitialiseProgress()
        {
            try
            {
                string CP = CommuneCP;
                string ville = CommuneName;
                int nombreTotal = 0;


                RefAdresseDAL refAdresseDAL = new RefAdresseDAL(Tools.DB.ConnexionName);

                if (rdParticuliers.Checked)
                {
                    nombreTotal = refAdresseDAL.GetNombreATraiter(CP, ville, RefAdresseDAL.ENUM_TYPEANNUAIRE.PART);
                }
                else if (rdProfessionnel.Checked)
                {
                    nombreTotal = refAdresseDAL.GetNombreATraiter(CP, ville, RefAdresseDAL.ENUM_TYPEANNUAIRE.PRO);
                }

                if (nombreTotal > 0)
                {
                    progressBarSpider.Value = 0;
                    progressBarSpider.Maximum = nombreTotal;
                    progressBarSpider.Step = 1;
                    progressBarSpider.PerformStep();

                    //SearchReferenceAddress();
                }
                else
                {
                    lblError.Text = "Aucune fiche à extraire pour la commune " + ville + " (" + CP + ")";
                    lblError.ForeColor = Color.Red;
                }

            }
            catch (Exception ex)
            {
                HandleExceptionMessage(new Exception("InitialisePageJaunes: " + ex.Message), false);
            }
        }

        public void AutoSelectCommune()
        {
            bool status = false;

            foreach (CCBoxItem item in ccbCP.CheckedItems)
            {
                CommuneCP = item.Value.ToString();
                if (CommuneCP.Length < 5)
                    CommuneCP = "0" + CommuneCP;

                CommuneName = item.Name.Substring(item.Name.IndexOf("-") + 1).Trim();

                if (CommuneList == null)
                    CommuneList = new HashSet<string>();

                if (CommuneList.Contains(CommuneCP + "-" + CommuneName) == false)
                {
                    CommuneList.Add(CommuneCP + "-" + CommuneName);

                    ResetReferenceAddress();

                    InitialiseProgress();

                    status = true;

                    break;
                }

            }

            if (status)
                SearchReferenceAddress();
            else
            {
                timerCheckIdle.Stop();
                progressBarSpider.Invoke(new Action(() => progressBarSpider.Value = progressBarSpider.Maximum));
                lblError.Invoke(new Action(() => lblError.Text = "Il n'y a plus des adresses à extraire pour " + CommuneName + " - " + CommuneCP));

                btnPlay.Enabled = true;
            }

        }

        public void SearchReferenceAddress()
        {
            try
            {
                string CP = CommuneCP;

                string ville = CommuneName;

                string sqlCommand = "SELECT * FROM RefAdresse " +
                                    "WHERE (Statut = " + (int)RefAdresseDAL.ENUM_STATUS.AWAITING + ") AND " +
                                    "(LTRIM(RTRIM(CP)) = '" + CP + "') AND " +
                                    "(LTRIM(RTRIM(Ville)) = '" + ville + "') AND ";

                if (rdParticuliers.Checked)
                {
                    sqlCommand = sqlCommand + "(TypeAnnuaire = '" + RefAdresseDAL.ENUM_TYPEANNUAIRE.PART.ToString() + "')";
                }
                else if (rdProfessionnel.Checked)
                {
                    sqlCommand = sqlCommand + "(TypeAnnuaire = '" + RefAdresseDAL.ENUM_TYPEANNUAIRE.PRO.ToString() + "')";
                }

                using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(Tools.DB.ConnexionName, CommandType.Text, sqlCommand))
                {
                    RefAdresseDAL refAdresseDal = new RefAdresseDAL(Tools.DB.ConnexionName);

                    if (dataReader.HasRows)
                    {
                        bool isLocked = true;
                        string searchAdresse = null;

                        while (dataReader.Read())
                        {
                            //progressBarSpider.PerformStep();
                            progressBarSpider.Invoke(new Action(() => progressBarSpider.PerformStep()));

                            _refAdresseEnCours = refAdresseDal.MakeRefAdresse(dataReader);
                            searchAdresse = _refAdresseEnCours.Adresse.Trim() + ", " + _refAdresseEnCours.CP.Trim() + " " + _refAdresseEnCours.Ville.Trim();

                            isLocked = refAdresseDal.IsRefAdresseBusy(_refAdresseEnCours);

                            if (isLocked == false)
                            {
                                break;
                            }
                        }

                        if (isLocked == false)
                        {
                            StartPageJaune(searchAdresse);
                        }

                    }
                    else
                    {
                        AutoSelectCommune();
                    }

                    SqlClientUtility.CloseConnection();
                }

            }
            catch (Exception ex)
            {
                HandleExceptionMessage(new Exception("SearchReferenceAddress: " + ex.Message), false);
            }

        }

        public void HandleExceptionMessage(Exception pException, bool pUpdateRefAddress)
        {
            try
            {
                string displayMessage = pException.Message;

                if (pUpdateRefAddress)
                    UpdateRefAddress(pException.Message, RefAdresseDAL.ENUM_STATUS.FAILED);

                //lblError.Text = displayMessage.Trim();
                //picError.Visible = true;
                lblError.Invoke(new Action(() => lblError.Text = displayMessage.Trim()));
                picError.Invoke(new Action(() => picError.Visible = true));

                Gmail gmail = new Gmail();
                gmail.SendEmail(_computerName + " : " + displayMessage.Trim());
            }
            catch (Exception exInner)
            {
                //lblError.Text = exInner.Message;
                //picError.Visible = true;

                lblError.Invoke(new Action(() => lblError.Text = exInner.Message));
                picError.Invoke(new Action(() => picError.Visible = true));
            }

        }

        private void InitialiseRobot()
        {
            try
            {
                string url = string.Empty;
                string searchAddress = string.Empty;

                //searchAddress = pSearchAddress;
                searchAddress = "Allée de Barcelone, Toulouse (31000)";
                //searchAddress = "testo royal, paris (75000)";

                var utf8 = Encoding.UTF8;
                byte[] utfBytes = utf8.GetBytes(searchAddress);
                searchAddress = utf8.GetString(utfBytes, 0, utfBytes.Length);
                searchAddress = searchAddress.Replace("'", " ");

                string strHtml = string.Empty;

                searchAddress = System.Web.HttpUtility.UrlEncode(searchAddress);

                if (rdParticuliers.Checked)
                {
                    url = PARTICULIER_URL + "ou=" + searchAddress + "&proximite=0";
                }
                else if (rdProfessionnel.Checked)
                {
                    url = PROFESSIONNEL_URL + "ou=" + searchAddress + "&proximite=0";
                }

                //chromeBrowser.LoadingStateChanged += OnLoadingStateChanged;
                chromeBrowser.Load(url);


            }
            catch (Exception ex)
            {
                HandleExceptionMessage(new Exception("StartPageJaunes: " + ex.Message), true);
            }

        }

        private void StartPageJaune(string pSearchAddress)
        {
            try
            {
                string url = string.Empty;
                string searchAddress = string.Empty;

                _compteur = 0;
                _pageNo = 1;

                //searchAddress = pSearchAddress;
                //pSearchAddress = "Rue de Rennes, Paris (75006)";
                //searchAddress = "testo royal, paris (75000)";
                //searchAddress = "ALLEE DES VITARELLES, 31100 Toulouse";

                var utf8 = Encoding.UTF8;
                byte[] utfBytes = utf8.GetBytes(pSearchAddress);
                searchAddress = utf8.GetString(utfBytes, 0, utfBytes.Length);
                searchAddress = searchAddress.Replace("'", " ");

                string strHtml = string.Empty;

                searchAddress = System.Web.HttpUtility.UrlEncode(searchAddress);

                if (rdParticuliers.Checked)
                {
                    //url = PARTICULIER_URL + "ou=" + searchAddress + "&proximite=0";

                    if (_refAdresseEnCours.PageExtrait > 1)
                        url = PARTICULIER_URL + "ou=" + searchAddress + "&proximite=0&page=" + _refAdresseEnCours.PageExtrait;
                    else
                        url = PARTICULIER_URL + "ou=" + searchAddress + "&proximite=0";

                }
                else if (rdProfessionnel.Checked)
                {
                    //url = PROFESSIONNEL_URL + "ou=" + searchAddress + "&proximite=0";

                    if (_refAdresseEnCours.PageExtrait > 1)
                        url = PROFESSIONNEL_URL + "ou=" + searchAddress + "&proximite=0&page=" + _refAdresseEnCours.PageExtrait;
                    else
                        url = PROFESSIONNEL_URL + "ou=" + searchAddress + "&proximite=0";
                }

                chromeBrowser.LoadingStateChanged += OnLoadingStateChanged;
                chromeBrowser.Load(url);


            }
            catch (Exception ex)
            {
                HandleExceptionMessage(new Exception("StartPageJaunes: " + ex.Message), true);
            }

        }

        private void StartExtraction(string pHtml)
        {
            try
            {
                _lesInfoExtrait = new List<InfoExtrait>();

                ExtractBlock(pHtml);

                dataGridInfoExtrait.Invoke(new Action(() => dataGridInfoExtrait.AutoGenerateColumns = false));
                //dataGridInfoExtrait.AutoGenerateColumns = false;
                dataGridInfoExtrait.Invoke(new Action(() => dataGridInfoExtrait.DataSource = _lesInfoExtrait));
                //dataGridInfoExtrait.DataSource = _lesInfoExtrait;

            }
            catch (Exception)
            {
                throw;
            }
        }

        private void ExtractBlock(string pHtml)
        {
            try
            {
                string pageJauneHtml = pHtml;
                pageJauneHtml = HttpUtility.HtmlDecode(pageJauneHtml);

                bool isFirstBlock = true;
                string startTag = @"<article",
                       endTag = "</article>",
                       visitCardBlock = string.Empty;
                int startPos = 0,
                    endPos = 0,
                    startTagLength = startTag.Length,
                    endTagLength = endTag.Length;

                int countException = 0;

                while (true)
                {
                    //Extracting the bloc of string containing communication details
                    startPos = pageJauneHtml.IndexOf(startTag, startPos, StringComparison.OrdinalIgnoreCase);
                    endPos = pageJauneHtml.IndexOf(endTag, (startPos + startTag.Length), StringComparison.OrdinalIgnoreCase);

                    if (startPos != -1 && endPos != -1)
                    {
                        visitCardBlock = pageJauneHtml.Substring(startPos, (endPos - startPos) + endTagLength);
                        startPos = endPos + endTagLength;

                        if (visitCardBlock != string.Empty)
                        {
                            try
                            {
                                ExtractBlockData(visitCardBlock);
                            }
                            catch (Exception)
                            {
                                countException = countException + 1;

                                if (countException == 10)
                                    throw;
                            }
                        }
                        else
                            throw new Exception("PagesJaunes a modifié son algorithme. \nCard de visit invalide. \nVeuillez contacter votre administrateur systeme.");
                    }
                    else if (startPos == -1 || endPos == -1)
                    {
                        if (isFirstBlock)
                        {
                            throw new Exception("PagesJaunes a modifié son algorithme. \nTag invalide pour extraire le bloc. \nVeuillez contacter votre administrateur systeme.");
                        }
                        break;
                    }

                    isFirstBlock = false;

                }

            }
            catch (Exception ex)
            {
                throw new Exception("ExtractionBloc: " + ex.Message);
            }

        }

        private void ExtractURL(string pHtml)
        {

            try
            {
                string pageJauneHtml = pHtml;
                pageJauneHtml = HttpUtility.HtmlDecode(pageJauneHtml);

                string startTag = @"<span",
                       middleTag = @"class=""pagination-compteur""",
                       endTag = "</span>",
                       paginationBlock = string.Empty,
                       URL = string.Empty;

                int startPos = 0,
                    middlePos = 0,
                    endPos = 0,
                    startTagLength = startTag.Length,
                    endTagLength = endTag.Length;

                middlePos = pageJauneHtml.IndexOf(middleTag, 0);
                startPos = pageJauneHtml.Substring(0, middlePos).LastIndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                endPos = pageJauneHtml.IndexOf(endTag, (startPos + startTag.Length), StringComparison.OrdinalIgnoreCase);

                if (startPos != -1 && endPos != -1)
                {
                    paginationBlock = pageJauneHtml.Substring(startPos, (endPos - startPos) + endTagLength);

                    //< span id = "SEL-compteur" class="pagination-compteur"><strong>Page 1</strong> / 17</span>
                    if (paginationBlock != string.Empty)
                    {
                        startPos = 0;
                        endPos = 0;

                        startTag = @"</strong> /";
                        endTag = "</span>";

                        startTagLength = startTag.Length;
                        endTagLength = endTag.Length;

                        startPos = paginationBlock.LastIndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                        endPos = paginationBlock.LastIndexOf(endTag, StringComparison.OrdinalIgnoreCase);

                        if (startPos != -1 && endPos != -1)
                        {
                            string strCompteur = paginationBlock.Substring(startPos + startTagLength, (endPos - (startPos + startTagLength)));
                            strCompteur = strCompteur.Trim();

                            _compteur = Convert.ToInt16(strCompteur);


                            startPos = 0;
                            endPos = 0;

                            startTag = @"<strong>Page";
                            endTag = "</strong>";

                            startTagLength = startTag.Length;
                            endTagLength = endTag.Length;

                            startPos = paginationBlock.LastIndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                            endPos = paginationBlock.LastIndexOf(endTag, StringComparison.OrdinalIgnoreCase);

                            if (startPos != -1 && endPos != -1)
                            {
                                string strPageNo = paginationBlock.Substring(startPos + startTagLength, (endPos - (startPos + startTagLength)));
                                strPageNo = strPageNo.Trim();

                                _pageNo = Convert.ToInt16(strPageNo);

                                _pageNo = _pageNo + 1;

                                if (_pageNo <= _compteur)
                                {
                                    string url = chromeBrowser.Address;

                                    if (_pageNo == 2)
                                        url = url + "&page=" + _pageNo;
                                    else
                                        url = url.Substring(0, url.LastIndexOf("=") + 1) + _pageNo;


                                    UpdateRefAddress(string.Empty, RefAdresseDAL.ENUM_STATUS.IN_PROGRESS);

                                    WaitExtraction();

                                    chromeBrowser.LoadingStateChanged += OnLoadingStateChanged;
                                    chromeBrowser.Load(url);
                                }
                                else if ((_pageNo - 1) == _compteur)
                                {
                                    UpdateRefAddress(string.Empty, RefAdresseDAL.ENUM_STATUS.TERMINATED);

                                    WaitExtraction();

                                    SearchReferenceAddress();
                                }

                            }
                            else
                            {
                                throw new Exception("Incapable d'extraire la valeur de la page courante");
                            }

                        }
                        else
                        {
                            throw new Exception("Incapable d'extraire le nombre total de la page");
                        }
                    }
                    else
                    {
                        throw new Exception("La section de la pagination est vide");
                    }

                }
                else
                {
                    throw new Exception("Incapable d'extraire la section de la pagination");
                }
            }
            catch (Exception ex)
            {
                Exception newEx = new Exception("ExtractionURL: " + "PagesJaunes a modifié son algorithme. " +
                                                "\n URL invalide. \n Veuillez contacter votre administrateur systeme. \n " + ex.Message);

                HandleExceptionMessage(newEx, true);
            }

        }

        private void ExtractBlockData(string pBlock)
        {
            try
            {
                InfoExtrait infoExtrait = new InfoExtrait();

                infoExtrait.TypeAnnuaire = ExtractTypeAnnuaire(pBlock);
                infoExtrait.Nom = ExtractNom(pBlock);
                string adresseComplet = ExtractAdresse(pBlock);
                infoExtrait = SplitAddress(adresseComplet, infoExtrait);
                infoExtrait.TypeSociete = ExtractTypeSociete(pBlock);
                infoExtrait.Tel = ExtractTel(pBlock);
                infoExtrait.Mobile = ExtractMobile(pBlock);

                if (infoExtrait.Tel == string.Empty && infoExtrait.Mobile == string.Empty)
                {
                    string fax = ExtractFax(pBlock);
                    if (fax == string.Empty)
                    {
                        string aucune = ExtractAucuneContact(pBlock);
                        if (aucune != string.Empty)
                        {
                            //throw new Exception("Le numéro de contact est indisponible");
                            infoExtrait.Tel = "Le numéro de contact est indisponible";
                        }
                        else
                        {
                            throw new Exception("PagesJaunes a modifié son algorithme. \nBalise incorrecte pour récupérer le numéro de téléphone du contact. \nVeuillez contacter votre administrateur systeme.");

                        }
                    }
                    else
                    {
                        infoExtrait.Tel = fax;
                    }
                }

                infoExtrait.RefAdresseId = _refAdresseEnCours.Id;
                infoExtrait.DateModifier = Tools.DB.GetCurrentDateTime();

                InfoExtraitDAL infoExtraitDAL = new InfoExtraitDAL(Tools.DB.ConnexionName);
                infoExtraitDAL.Insert(infoExtrait);

                _lesInfoExtrait.Add(infoExtrait);
                _nombreContactExtrait = _nombreContactExtrait + 1;

            }
            catch (Exception ex)
            {
                throw new Exception("ExtractionBlockData: " + ex.Message);
            }

        }

        private string ExtractTypeAnnuaire(string pBlock)
        {
            string typeAnnuaire = string.Empty;

            try
            {
                string startTag = string.Empty;
                int startPos = 0;

                startTag = @"bi-pro";
                startPos = 0;

                startPos = pBlock.IndexOf(startTag, startPos, StringComparison.OrdinalIgnoreCase);
                if (startPos > 0)
                {
                    typeAnnuaire = "PRO";
                }
                else
                {
                    startTag = @"bi-part";
                    startPos = 0;

                    startPos = pBlock.IndexOf(startTag, startPos, StringComparison.OrdinalIgnoreCase);
                    if (startPos > 0)
                    {
                        typeAnnuaire = "PART";
                    }
                    else
                    {
                        throw new Exception("PagesJaunes a modifié son algorithme. \n" +
                                            "Tag invalide pour extraire le type d'annuaire. \n" +
                                            "Veuillez consulter votre interface d'extraction pour vérifier la qualité des données extraites. \n" +
                                            "Si vous avez identifié que le problème persiste pour le reste de l'extraction dans la grille de données. \n" +
                                            "Veuillez contacter votre administrateur systeme.");
                    }

                }

            }
            catch (Exception ex)
            {
                throw new Exception("ExtractionTypeAnnuaire: " + ex.Message);
            }

            return typeAnnuaire;
        }

        private string ExtractNom(string pBlock)
        {
            string nom = string.Empty;

            try
            {
                string startTag = @"<a",
                       middleTag = @"class=""bi-pos-links pj-lb pj-link""",
                       endTag = @"</a>";

                int startPos = 0,
                    middlePos = 0,
                    endPos = 0,
                    startTagLength = startTag.Length,
                    endTagLength = endTag.Length;

                middlePos = pBlock.IndexOf(middleTag, 0);
                startPos = pBlock.Substring(0, middlePos).LastIndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                endPos = pBlock.IndexOf(endTag, (startPos + startTag.Length), StringComparison.OrdinalIgnoreCase);

                if (startPos != -1 && endPos != -1)
                {
                    pBlock = pBlock.Substring(startPos, (endPos - startPos) + endTagLength);

                    if (pBlock != string.Empty)
                    {
                        startPos = 0;
                        endPos = 0;

                        startTag = @"title=""";
                        endTag = @"""";

                        startTagLength = startTag.Length;
                        endTagLength = endTag.Length;

                        startPos = pBlock.IndexOf(startTag, 0, StringComparison.OrdinalIgnoreCase);
                        endPos = pBlock.IndexOf(endTag, (startPos + startTag.Length), StringComparison.OrdinalIgnoreCase);

                        if (startPos != -1 && endPos != -1)
                        {
                            nom = pBlock.Substring(startPos + startTagLength, (endPos - (startPos + startTagLength)));

                            if (nom == string.Empty)
                                throw new Exception("PagesJaunes a modifié son algorithme. \n" +
                                                    "Tag invalide pour extraire le nom. \n" +
                                                    "Veuillez consulter votre interface d'extraction pour vérifier la qualité des données extraites. \n" +
                                                    "Si vous avez identifié que le problème persiste pour le reste de l'extraction dans la grille de données. \n" +
                                                    "Veuillez contacter votre administrateur systeme.");
                        }
                        else
                        {
                            throw new Exception("PagesJaunes a modifié son algorithme. \n" +
                                                "Tag invalide pour extraire le nom. \n" +
                                                "Veuillez consulter votre interface d'extraction pour vérifier la qualité des données extraites. \n" +
                                                "Si vous avez identifié que le problème persiste pour le reste de l'extraction dans la grille de données. \n" +
                                                "Veuillez contacter votre administrateur systeme.");
                        }

                    }
                    else
                    {
                        throw new Exception("PagesJaunes a modifié son algorithme. \n" +
                                            "Tag invalide pour extraire le nom. \n" +
                                            "Veuillez consulter votre interface d'extraction pour vérifier la qualité des données extraites. \n" +
                                            "Si vous avez identifié que le problème persiste pour le reste de l'extraction dans la grille de données. \n" +
                                            "Veuillez contacter votre administrateur systeme.");
                    }


                }
                else
                {
                    throw new Exception("PagesJaunes a modifié son algorithme. \n" +
                                        "Tag invalide pour extraire le nom. \n" +
                                        "Veuillez consulter votre interface d'extraction pour vérifier la qualité des données extraites. \n" +
                                        "Si vous avez identifié que le problème persiste pour le reste de l'extraction dans la grille de données. \n" +
                                        "Veuillez contacter votre administrateur systeme.");
                }


            }
            catch (Exception ex)
            {
                throw new Exception("ExtractionNom: " + ex.Message);
            }

            return nom;
        }

        private string ExtractAdresse(string pBlock)
        {
            string adresse = string.Empty;

            try
            {
                string startTag = @"<a",
                       middleTag = @"class=""adresse pj-lb pj-link""",
                       endTag = @"</a>";

                int startPos = 0,
                    middlePos = 0,
                    endPos = 0,
                    startTagLength = startTag.Length,
                    endTagLength = endTag.Length;

                middlePos = pBlock.IndexOf(middleTag, 0);

                if (middlePos != -1)
                {
                    startPos = pBlock.Substring(0, middlePos).LastIndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                    endPos = pBlock.IndexOf(endTag, (startPos + startTag.Length), StringComparison.OrdinalIgnoreCase);

                    if (startPos != -1 && endPos != -1)
                    {
                        pBlock = pBlock.Substring(startPos, (endPos - startPos) + endTagLength);

                        if (pBlock != string.Empty)
                        {
                            startPos = 0;
                            endPos = 0;

                            startTag = @">";
                            endTag = @"</a>";

                            startTagLength = startTag.Length;
                            endTagLength = endTag.Length;

                            startPos = pBlock.IndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                            endPos = pBlock.IndexOf(endTag, StringComparison.OrdinalIgnoreCase);

                            if (startPos != -1 && endPos != -1)
                            {
                                adresse = pBlock.Substring(startPos + startTagLength, (endPos - (startPos + startTagLength))).Trim();

                                if (adresse == string.Empty)
                                    throw new Exception("PagesJaunes a modifié son algorithme. \nTag invalide pour extraire l'adresse. \nVeuillez contacter votre administrateur systeme.");

                            }
                            else
                            {
                                throw new Exception("PagesJaunes a modifié son algorithme. \nTag invalide pour extraire l'adresse. \nVeuillez contacter votre administrateur systeme.");
                            }

                        }
                        else
                        {
                            throw new Exception("PagesJaunes a modifié son algorithme. \nTag invalide pour extraire l'adresse. \nVeuillez contacter votre administrateur systeme.");
                        }


                    }
                    else
                    {
                        throw new Exception("PagesJaunes a modifié son algorithme. \nTag invalide pour extraire l'adresse. \nVeuillez contacter votre administrateur systeme.");
                    }
                }
                else
                {
                    startTag = @"<a";
                    middleTag = @"class=""adresse pj-link""";
                    endTag = @"</a>";

                    startPos = 0;
                    middlePos = 0;
                    endPos = 0;
                    startTagLength = startTag.Length;
                    endTagLength = endTag.Length;

                    middlePos = pBlock.IndexOf(middleTag, 0);

                    if (middlePos != -1)
                    {
                        startPos = pBlock.Substring(0, middlePos).LastIndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                        endPos = pBlock.IndexOf(endTag, (startPos + startTag.Length), StringComparison.OrdinalIgnoreCase);

                        if (startPos != -1 && endPos != -1)
                        {
                            pBlock = pBlock.Substring(startPos, (endPos - startPos) + endTagLength);

                            if (pBlock != string.Empty)
                            {
                                startPos = 0;
                                endPos = 0;

                                startTag = @""">";
                                endTag = @"</a>";

                                startTagLength = startTag.Length;
                                endTagLength = endTag.Length;

                                startPos = pBlock.LastIndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                                endPos = pBlock.LastIndexOf(endTag, StringComparison.OrdinalIgnoreCase);

                                if (startPos != -1 && endPos != -1)
                                {
                                    adresse = pBlock.Substring(startPos + startTagLength, (endPos - (startPos + startTagLength))).Trim();

                                    if (adresse == string.Empty)
                                        throw new Exception("PagesJaunes a modifié son algorithme. \nTag invalide pour extraire l'adresse. \nVeuillez contacter votre administrateur systeme.");

                                }
                                else
                                {
                                    throw new Exception("PagesJaunes a modifié son algorithme. \nTag invalide pour extraire l'adresse. \nVeuillez contacter votre administrateur systeme.");
                                }

                            }
                            else
                            {
                                throw new Exception("PagesJaunes a modifié son algorithme. \nTag invalide pour extraire l'adresse. \nVeuillez contacter votre administrateur systeme.");
                            }


                        }
                        else
                        {
                            throw new Exception("PagesJaunes a modifié son algorithme. \nTag invalide pour extraire l'adresse. \nVeuillez contacter votre administrateur systeme.");
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                throw new Exception("ExtractionAdresse: " + ex.Message);
            }

            return adresse;
        }

        private string ExtractTypeSociete(string pBlock)
        {
            string societe = string.Empty;

            try
            {
                string startTag = @"<a",
                       middleTag = @"class=""activites pj-lb pj-link""",
                       endTag = @"</a>";

                int startPos = 0,
                    middlePos = 0,
                    endPos = 0,
                    startTagLength = startTag.Length,
                    endTagLength = endTag.Length;

                middlePos = pBlock.IndexOf(middleTag, 0);
                if (middlePos != -1)
                {
                    startPos = pBlock.Substring(0, middlePos).LastIndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                    endPos = pBlock.IndexOf(endTag, (startPos + startTag.Length), StringComparison.OrdinalIgnoreCase);

                    if (startPos != -1 && endPos != -1)
                    {
                        pBlock = pBlock.Substring(startPos, (endPos - startPos) + endTagLength);

                        if (pBlock != string.Empty)
                        {
                            startPos = 0;
                            endPos = 0;

                            startTag = @">";
                            endTag = @"</a>";

                            startTagLength = startTag.Length;
                            endTagLength = endTag.Length;

                            startPos = pBlock.IndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                            endPos = pBlock.IndexOf(endTag, StringComparison.OrdinalIgnoreCase);

                            if (startPos != -1 && endPos != -1)
                            {
                                societe = pBlock.Substring(startPos + startTagLength, (endPos - (startPos + startTagLength))).Trim();
                            }

                        }

                    }

                }

            }
            catch (Exception ex)
            {
                throw new Exception("ExtractionTypeSociété: " + ex.Message);
            }

            return societe;
        }

        private string ExtractTel(string pBlock)
        {
            string tel = string.Empty;

            try
            {
                string startTag = @"<div",
                       middleTag = @"Tél",
                       endTag = @"</div>";

                int startPos = 0,
                    middlePos = 0,
                    endPos = 0,
                    startTagLength = startTag.Length,
                    endTagLength = endTag.Length;

                middlePos = pBlock.IndexOf(middleTag, 0);
                if (middlePos != -1)
                {
                    startPos = pBlock.Substring(0, middlePos).LastIndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                    endPos = pBlock.IndexOf(endTag, (startPos + startTag.Length), StringComparison.OrdinalIgnoreCase);

                    if (startPos != -1 && endPos != -1)
                    {
                        pBlock = pBlock.Substring(startPos, (endPos - startPos) + endTagLength);

                        if (pBlock != string.Empty)
                        {
                            startPos = 0;
                            endPos = 0;

                            startTag = @"title=""";
                            endTag = @"""";

                            startTagLength = startTag.Length;
                            endTagLength = endTag.Length;

                            startPos = pBlock.LastIndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                            endPos = pBlock.IndexOf(endTag, (startPos + startTagLength), StringComparison.OrdinalIgnoreCase);

                            if (startPos != -1 && endPos != -1)
                            {
                                tel = pBlock.Substring(startPos + startTagLength, (endPos - (startPos + startTagLength))).Trim();
                            }
                            else
                            {
                                startPos = 0;
                                endPos = 0;

                                startTag = @"<div class=""num num-arcep"">";
                                endTag = @"</div>";

                                startTagLength = startTag.Length;
                                endTagLength = endTag.Length;

                                startPos = pBlock.LastIndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                                endPos = pBlock.IndexOf(endTag, (startPos + startTagLength), StringComparison.OrdinalIgnoreCase);

                                if (startPos != -1 && endPos != -1)
                                {
                                    tel = pBlock.Substring(startPos + startTagLength, (endPos - (startPos + startTagLength))).Trim();
                                }
                            }

                        }

                    }

                }
                else
                {
                    startTag = @"<div";
                    middleTag = @"class=""num""";
                    endTag = @"</div>";

                    startPos = 0;
                    middlePos = 0;
                    endPos = 0;
                    startTagLength = startTag.Length;
                    endTagLength = endTag.Length;

                    middlePos = pBlock.IndexOf(middleTag, 0);

                    if (middlePos != -1)
                    {
                        startPos = pBlock.Substring(0, middlePos).LastIndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                        endPos = pBlock.IndexOf(endTag, (startPos + startTag.Length), StringComparison.OrdinalIgnoreCase);

                        if (startPos != -1 && endPos != -1)
                        {
                            pBlock = pBlock.Substring(startPos, (endPos - startPos) + endTagLength);

                            if (pBlock != string.Empty)
                            {
                                startPos = 0;
                                endPos = 0;

                                startTag = @"title=""";
                                endTag = @"""";

                                startTagLength = startTag.Length;
                                endTagLength = endTag.Length;

                                startPos = pBlock.LastIndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                                endPos = pBlock.IndexOf(endTag, (startPos + startTagLength), StringComparison.OrdinalIgnoreCase);

                                if (startPos != -1 && endPos != -1)
                                {
                                    tel = pBlock.Substring(startPos + startTagLength, (endPos - (startPos + startTagLength))).Trim();
                                }

                            }

                        }

                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception("ExtractionTel: " + ex.Message);
            }

            return tel;
        }

        private string ExtractMobile(string pBlock)
        {
            string mobile = string.Empty;

            try
            {
                string startTag = @"<div",
                       middleTag = @"Mobile",
                       endTag = @"</div>";

                int startPos = 0,
                    middlePos = 0,
                    endPos = 0,
                    startTagLength = startTag.Length,
                    endTagLength = endTag.Length;

                middlePos = pBlock.IndexOf(middleTag, 0);
                if (middlePos != -1)
                {
                    startPos = pBlock.Substring(0, middlePos).LastIndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                    endPos = pBlock.IndexOf(endTag, (startPos + startTag.Length), StringComparison.OrdinalIgnoreCase);

                    if (startPos != -1 && endPos != -1)
                    {
                        pBlock = pBlock.Substring(startPos, (endPos - startPos) + endTagLength);

                        if (pBlock != string.Empty)
                        {
                            startPos = 0;
                            endPos = 0;

                            startTag = @"title=""";
                            endTag = @"""";

                            startTagLength = startTag.Length;
                            endTagLength = endTag.Length;

                            startPos = pBlock.LastIndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                            endPos = pBlock.IndexOf(endTag, (startPos + startTagLength), StringComparison.OrdinalIgnoreCase);

                            if (startPos != -1 && endPos != -1)
                            {
                                mobile = pBlock.Substring(startPos + startTagLength, (endPos - (startPos + startTagLength))).Trim();
                            }

                        }

                    }

                }

            }
            catch (Exception ex)
            {
                throw new Exception("ExtractionMobile: " + ex.Message);
            }

            return mobile;
        }

        private string ExtractFax(string pBlock)
        {
            string fax = string.Empty;

            try
            {
                string startTag = @"<div",
                       middleTag = @"Fax",
                       endTag = @"</div>";

                int startPos = 0,
                    middlePos = 0,
                    endPos = 0,
                    startTagLength = startTag.Length,
                    endTagLength = endTag.Length;

                middlePos = pBlock.IndexOf(middleTag, 0);
                if (middlePos != -1)
                {
                    startPos = pBlock.Substring(0, middlePos).LastIndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                    endPos = pBlock.IndexOf(endTag, (startPos + startTag.Length), StringComparison.OrdinalIgnoreCase);

                    if (startPos != -1 && endPos != -1)
                    {
                        pBlock = pBlock.Substring(startPos, (endPos - startPos) + endTagLength);

                        if (pBlock != string.Empty)
                        {
                            startPos = 0;
                            endPos = 0;

                            startTag = @"title=""";
                            endTag = @"""";

                            startTagLength = startTag.Length;
                            endTagLength = endTag.Length;

                            startPos = pBlock.LastIndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                            endPos = pBlock.IndexOf(endTag, (startPos + startTagLength), StringComparison.OrdinalIgnoreCase);

                            if (startPos != -1 && endPos != -1)
                            {
                                fax = pBlock.Substring(startPos + startTagLength, (endPos - (startPos + startTagLength))).Trim();
                            }

                        }

                    }

                }

            }
            catch (Exception ex)
            {
                throw new Exception("ExtractionFax: " + ex.Message);
            }

            return fax;
        }

        private string ExtractAucuneContact(string pBlock)
        {
            string aucune = string.Empty;

            try
            {
                string startTag = @"<div",
                       middleTag = @"class=""item no-coordonnee"">",
                       endTag = @"</div>";

                int startPos = 0,
                    middlePos = 0,
                    endPos = 0,
                    startTagLength = startTag.Length,
                    endTagLength = endTag.Length;

                middlePos = pBlock.IndexOf(middleTag, 0);
                if (middlePos != -1)
                {
                    startPos = pBlock.Substring(0, middlePos).LastIndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                    endPos = pBlock.IndexOf(endTag, (startPos + startTag.Length), StringComparison.OrdinalIgnoreCase);

                    if (startPos != -1 && endPos != -1)
                    {
                        pBlock = pBlock.Substring(startPos, (endPos - startPos) + endTagLength);

                        if (pBlock != string.Empty)
                        {
                            startPos = 0;
                            endPos = 0;

                            startTag = @">";
                            endTag = @"</div>";

                            startTagLength = startTag.Length;
                            endTagLength = endTag.Length;

                            startPos = pBlock.IndexOf(startTag, StringComparison.OrdinalIgnoreCase);
                            endPos = pBlock.IndexOf(endTag, (startPos + startTagLength), StringComparison.OrdinalIgnoreCase);

                            if (startPos != -1 && endPos != -1)
                            {
                                aucune = pBlock.Substring(startPos + startTagLength, (endPos - (startPos + startTagLength))).Trim();
                            }

                        }

                    }

                }

            }
            catch (Exception ex)
            {
                throw new Exception("ExtractAucuneContact: " + ex.Message);
            }

            return aucune;
        }

        private InfoExtrait SplitAddress(string pAdresseComplet, InfoExtrait pInfoExtrait)
        {
            try
            {

                string[] words = null;

                pAdresseComplet = pAdresseComplet.Replace(",", "");
                pAdresseComplet = pAdresseComplet.Replace("\n", " ");
                words = pAdresseComplet.Split(' ');


                foreach (string word in words)
                {
                    if (word.Length == 5 && IsNumeric(word))
                    {
                        pInfoExtrait.CP = word;
                        pInfoExtrait.Adresse = pAdresseComplet.Substring(0, pAdresseComplet.IndexOf(word)).Trim();
                        pInfoExtrait.Ville = pAdresseComplet.Substring(pAdresseComplet.IndexOf(word) + 5).Trim();

                        break;
                    }
                }

                //if (pInfoExtrait.CP == string.Empty || pInfoExtrait.Adresse == string.Empty || pInfoExtrait.Ville == string.Empty)
                //throw new Exception("PagesJaunes a modifié son algorithme. \n" +
                //                    "Balise invalide pour extraire l'adresse. \n" +
                //                    "Veuillez consulter votre interface d'extraction pour vérifier la qualité des données extraites. \n" +
                //                    "Si vous avez identifié que le problème persiste pour le reste de l'extraction dans la grille de données. \n" +
                //                    "Veuillez contacter votre administrateur systeme.");


            }
            catch (Exception ex)
            {
                throw new Exception("SplitAdresse: " + ex.Message);
            }

            return pInfoExtrait;

        }

        public bool IsNumeric(string pInput)
        {
            int number;
            return int.TryParse(pInput, out number);
        }

        private void UpdateRefAddress(string pErrorMessage, RefAdresseDAL.ENUM_STATUS pStatut)
        {
            try
            {
                RefAdresseDAL refAdresseDal = new RefAdresseDAL(Tools.DB.ConnexionName);

                _refAdresseEnCours.PageTotal = _compteur;
                _refAdresseEnCours.PageExtrait = (_pageNo - 1);
                _refAdresseEnCours.Erreur = pErrorMessage;
                _refAdresseEnCours.Statut = (int)pStatut;
                _refAdresseEnCours.DateModifier = Tools.DB.GetCurrentDateTime();

                refAdresseDal.Update(_refAdresseEnCours);
            }
            catch (Exception)
            {

            }
        }

        private void DisplayCommune()
        {
            try
            {
                RefAdresseDAL refAdresseDAL = new RefAdresseDAL(Tools.DB.ConnexionName);

                DataTable deptDataSet = refAdresseDAL.GetCPCommune();
                // ... Loop over all rows.
                foreach (DataRow row in deptDataSet.Rows)
                {
                    CCBoxItem item = new CCBoxItem(row["Adresse"].ToString(), Convert.ToInt32(row["CodePostal"]));
                    ccbCP.Items.Add(item);
                }

                ccbCP.DisplayMember = "Name";
                ccbCP.ValueMember = "Value";

            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                picError.Visible = true;
            }

        }

        private bool ResetReferenceAddress()
        {
            bool status = false;

            try
            {

                string CP = CommuneCP;
                string ville = CommuneName;

                //ville = ville.Substring(ville.IndexOf("-") + 1).Trim();

                RefAdresseDAL refAdresseDal = new RefAdresseDAL(Tools.DB.ConnexionName);

                if (rdParticuliers.Checked)
                {                    
                    status = refAdresseDal.ResetFichiersEnEchec(CP, ville, RefAdresseDAL.ENUM_TYPEANNUAIRE.PART.ToString(), chkResetEchec.Checked);
                }
                else if (rdProfessionnel.Checked)
                {
                    status = refAdresseDal.ResetFichiersEnEchec(CP, ville, RefAdresseDAL.ENUM_TYPEANNUAIRE.PRO.ToString(), chkResetEchec.Checked);
                }

            }
            catch (Exception)
            {

            }

            return status;
        }

        private void frmExtractionWeb_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void frmExtractionWeb_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //SearchReferenceAddress();
            //Gmail gmail = new Gmail();
            //gmail.SendEmail("Hello");   

        }

    }
}
