﻿namespace WebSpider
{
    partial class frmExtractionWeb
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExtractionWeb));
            this.grpCritère = new System.Windows.Forms.GroupBox();
            this.ccbCP = new CheckComboBoxTest.CheckedComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.chkResetEchec = new System.Windows.Forms.CheckBox();
            this.btnExtract = new System.Windows.Forms.Button();
            this.lblError = new System.Windows.Forms.Label();
            this.lblCommune = new System.Windows.Forms.Label();
            this.lblTypeAnnuaire = new System.Windows.Forms.Label();
            this.rdProfessionnel = new System.Windows.Forms.RadioButton();
            this.rdParticuliers = new System.Windows.Forms.RadioButton();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.picError = new System.Windows.Forms.PictureBox();
            this.progressBarSpider = new System.Windows.Forms.ProgressBar();
            this.dataGridInfoExtrait = new System.Windows.Forms.DataGridView();
            this.TypeAnnuaire = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.Nom = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.Adresse = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.CP = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.Ville = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.Tel = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.Mobile = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.TypeSociete = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.panelChrome = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.grpCritère.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridInfoExtrait)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpCritère
            // 
            this.grpCritère.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.grpCritère.Controls.Add(this.ccbCP);
            this.grpCritère.Controls.Add(this.button1);
            this.grpCritère.Controls.Add(this.chkResetEchec);
            this.grpCritère.Controls.Add(this.btnExtract);
            this.grpCritère.Controls.Add(this.lblError);
            this.grpCritère.Controls.Add(this.lblCommune);
            this.grpCritère.Controls.Add(this.lblTypeAnnuaire);
            this.grpCritère.Controls.Add(this.rdProfessionnel);
            this.grpCritère.Controls.Add(this.rdParticuliers);
            this.grpCritère.Controls.Add(this.btnStop);
            this.grpCritère.Controls.Add(this.btnPlay);
            this.grpCritère.Controls.Add(this.picError);
            this.grpCritère.Location = new System.Drawing.Point(6, 2);
            this.grpCritère.Name = "grpCritère";
            this.grpCritère.Size = new System.Drawing.Size(1046, 90);
            this.grpCritère.TabIndex = 22;
            this.grpCritère.TabStop = false;
            // 
            // ccbCP
            // 
            this.ccbCP.CheckOnClick = true;
            this.ccbCP.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.ccbCP.DropDownHeight = 1;
            this.ccbCP.FormattingEnabled = true;
            this.ccbCP.IntegralHeight = false;
            this.ccbCP.ItemHeight = 23;
            this.ccbCP.Location = new System.Drawing.Point(153, 35);
            this.ccbCP.MaxDropDownItems = 25;
            this.ccbCP.Name = "ccbCP";
            this.ccbCP.Size = new System.Drawing.Size(295, 29);
            this.ccbCP.TabIndex = 30;
            this.ccbCP.ValueSeparator = ", ";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(782, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(58, 57);
            this.button1.TabIndex = 29;
            this.button1.Text = "Mail Test";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // chkResetEchec
            // 
            this.chkResetEchec.AutoSize = true;
            this.chkResetEchec.Font = new System.Drawing.Font("Cambria", 9.75F);
            this.chkResetEchec.Location = new System.Drawing.Point(155, 65);
            this.chkResetEchec.Name = "chkResetEchec";
            this.chkResetEchec.Size = new System.Drawing.Size(197, 19);
            this.chkResetEchec.TabIndex = 28;
            this.chkResetEchec.Text = "Réinitialisation des extractions";
            this.chkResetEchec.UseVisualStyleBackColor = true;
            // 
            // btnExtract
            // 
            this.btnExtract.Image = global::WebSpider.Properties.Resources.Extract;
            this.btnExtract.Location = new System.Drawing.Point(985, 18);
            this.btnExtract.Name = "btnExtract";
            this.btnExtract.Size = new System.Drawing.Size(58, 58);
            this.btnExtract.TabIndex = 27;
            this.btnExtract.UseVisualStyleBackColor = true;
            this.btnExtract.Click += new System.EventHandler(this.btnExtract_Click);
            this.btnExtract.MouseLeave += new System.EventHandler(this.btnExtract_MouseLeave);
            this.btnExtract.MouseHover += new System.EventHandler(this.btnExtract_MouseHover);
            // 
            // lblError
            // 
            this.lblError.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(502, 19);
            this.lblError.MaximumSize = new System.Drawing.Size(355, 68);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(105, 17);
            this.lblError.TabIndex = 25;
            this.lblError.Text = "Message Erreur";
            // 
            // lblCommune
            // 
            this.lblCommune.AutoSize = true;
            this.lblCommune.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCommune.Location = new System.Drawing.Point(152, 17);
            this.lblCommune.Name = "lblCommune";
            this.lblCommune.Size = new System.Drawing.Size(159, 15);
            this.lblCommune.TabIndex = 24;
            this.lblCommune.Text = "Code Postal et Commune:";
            // 
            // lblTypeAnnuaire
            // 
            this.lblTypeAnnuaire.AutoSize = true;
            this.lblTypeAnnuaire.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTypeAnnuaire.Location = new System.Drawing.Point(17, 16);
            this.lblTypeAnnuaire.Name = "lblTypeAnnuaire";
            this.lblTypeAnnuaire.Size = new System.Drawing.Size(111, 15);
            this.lblTypeAnnuaire.TabIndex = 22;
            this.lblTypeAnnuaire.Text = "Type D\'annuaire:";
            // 
            // rdProfessionnel
            // 
            this.rdProfessionnel.AutoSize = true;
            this.rdProfessionnel.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdProfessionnel.Location = new System.Drawing.Point(20, 62);
            this.rdProfessionnel.Name = "rdProfessionnel";
            this.rdProfessionnel.Size = new System.Drawing.Size(110, 19);
            this.rdProfessionnel.TabIndex = 4;
            this.rdProfessionnel.Text = "Professionnels ";
            this.rdProfessionnel.UseVisualStyleBackColor = true;
            // 
            // rdParticuliers
            // 
            this.rdParticuliers.AutoSize = true;
            this.rdParticuliers.Checked = true;
            this.rdParticuliers.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdParticuliers.Location = new System.Drawing.Point(20, 36);
            this.rdParticuliers.Name = "rdParticuliers";
            this.rdParticuliers.Size = new System.Drawing.Size(89, 19);
            this.rdParticuliers.TabIndex = 3;
            this.rdParticuliers.TabStop = true;
            this.rdParticuliers.Text = "Particuliers";
            this.rdParticuliers.UseVisualStyleBackColor = true;
            // 
            // btnStop
            // 
            this.btnStop.Image = global::WebSpider.Properties.Resources.Stop;
            this.btnStop.Location = new System.Drawing.Point(920, 17);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(58, 58);
            this.btnStop.TabIndex = 2;
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            this.btnStop.MouseLeave += new System.EventHandler(this.btnStop_MouseLeave);
            this.btnStop.MouseHover += new System.EventHandler(this.btnStop_MouseHover);
            // 
            // btnPlay
            // 
            this.btnPlay.Image = global::WebSpider.Properties.Resources.Play;
            this.btnPlay.Location = new System.Drawing.Point(856, 17);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(58, 58);
            this.btnPlay.TabIndex = 0;
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            this.btnPlay.MouseLeave += new System.EventHandler(this.btnPlay_MouseLeave);
            this.btnPlay.MouseHover += new System.EventHandler(this.btnPlay_MouseHover);
            // 
            // picError
            // 
            this.picError.Image = global::WebSpider.Properties.Resources.error;
            this.picError.Location = new System.Drawing.Point(454, 16);
            this.picError.Name = "picError";
            this.picError.Size = new System.Drawing.Size(45, 45);
            this.picError.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picError.TabIndex = 26;
            this.picError.TabStop = false;
            // 
            // progressBarSpider
            // 
            this.progressBarSpider.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarSpider.Location = new System.Drawing.Point(4, 3);
            this.progressBarSpider.Name = "progressBarSpider";
            this.progressBarSpider.Size = new System.Drawing.Size(1037, 23);
            this.progressBarSpider.TabIndex = 22;
            // 
            // dataGridInfoExtrait
            // 
            this.dataGridInfoExtrait.AllowUserToAddRows = false;
            this.dataGridInfoExtrait.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.dataGridInfoExtrait.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridInfoExtrait.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridInfoExtrait.BackgroundColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dataGridInfoExtrait.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridInfoExtrait.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TypeAnnuaire,
            this.Nom,
            this.Adresse,
            this.CP,
            this.Ville,
            this.Tel,
            this.Mobile,
            this.TypeSociete});
            this.dataGridInfoExtrait.Location = new System.Drawing.Point(4, 3);
            this.dataGridInfoExtrait.Name = "dataGridInfoExtrait";
            this.dataGridInfoExtrait.ReadOnly = true;
            this.dataGridInfoExtrait.Size = new System.Drawing.Size(1035, 196);
            this.dataGridInfoExtrait.TabIndex = 0;
            // 
            // TypeAnnuaire
            // 
            this.TypeAnnuaire.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TypeAnnuaire.DataPropertyName = "TypeAnnuaire";
            this.TypeAnnuaire.HeaderText = "Type Annuaire";
            this.TypeAnnuaire.Name = "TypeAnnuaire";
            this.TypeAnnuaire.ReadOnly = true;
            // 
            // Nom
            // 
            this.Nom.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nom.DataPropertyName = "Nom";
            this.Nom.HeaderText = "Nom";
            this.Nom.Name = "Nom";
            this.Nom.ReadOnly = true;
            // 
            // Adresse
            // 
            this.Adresse.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Adresse.DataPropertyName = "Adresse";
            this.Adresse.HeaderText = "Adresse";
            this.Adresse.Name = "Adresse";
            this.Adresse.ReadOnly = true;
            // 
            // CP
            // 
            this.CP.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CP.DataPropertyName = "CP";
            this.CP.HeaderText = "CP";
            this.CP.Name = "CP";
            this.CP.ReadOnly = true;
            // 
            // Ville
            // 
            this.Ville.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Ville.DataPropertyName = "Ville";
            this.Ville.HeaderText = "Ville";
            this.Ville.Name = "Ville";
            this.Ville.ReadOnly = true;
            // 
            // Tel
            // 
            this.Tel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Tel.DataPropertyName = "Tel";
            this.Tel.HeaderText = "Tel";
            this.Tel.Name = "Tel";
            this.Tel.ReadOnly = true;
            // 
            // Mobile
            // 
            this.Mobile.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Mobile.DataPropertyName = "Mobile";
            this.Mobile.HeaderText = "Mobile";
            this.Mobile.Name = "Mobile";
            this.Mobile.ReadOnly = true;
            // 
            // TypeSociete
            // 
            this.TypeSociete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TypeSociete.DataPropertyName = "TypeSociete";
            this.TypeSociete.HeaderText = "TypeSociete";
            this.TypeSociete.Name = "TypeSociete";
            this.TypeSociete.ReadOnly = true;
            // 
            // panelChrome
            // 
            this.panelChrome.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelChrome.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChrome.Location = new System.Drawing.Point(5, 26);
            this.panelChrome.Name = "panelChrome";
            this.panelChrome.Size = new System.Drawing.Size(1034, 197);
            this.panelChrome.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Location = new System.Drawing.Point(7, 98);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.progressBarSpider);
            this.splitContainer1.Panel1.Controls.Add(this.panelChrome);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dataGridInfoExtrait);
            this.splitContainer1.Size = new System.Drawing.Size(1046, 456);
            this.splitContainer1.SplitterDistance = 228;
            this.splitContainer1.SplitterWidth = 8;
            this.splitContainer1.TabIndex = 26;
            // 
            // frmExtractionWeb
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1059, 562);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.grpCritère);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmExtractionWeb";
            this.Text = "ExtractionWeb";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmExtractionWeb_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmExtractionWeb_FormClosed);
            this.Load += new System.EventHandler(this.frmExtractionWeb_Load);
            this.grpCritère.ResumeLayout(false);
            this.grpCritère.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridInfoExtrait)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox grpCritère;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.RadioButton rdProfessionnel;
        private System.Windows.Forms.RadioButton rdParticuliers;
        private System.Windows.Forms.Label lblTypeAnnuaire;
        private System.Windows.Forms.Label lblCommune;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.PictureBox picError;
        private System.Windows.Forms.Button btnExtract;
        private System.Windows.Forms.ProgressBar progressBarSpider;
        private System.Windows.Forms.DataGridView dataGridInfoExtrait;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn TypeAnnuaire;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn Nom;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn Adresse;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn CP;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn Ville;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn Tel;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn Mobile;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn TypeSociete;
        private System.Windows.Forms.CheckBox chkResetEchec;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panelChrome;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private CheckComboBoxTest.CheckedComboBox ccbCP;
    }
}