using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using WebSpider_DLL.DAL;
using System.IO;
using WebSpider_DLL;

namespace WebSpider
{
    public partial class frmExtractionData : DevComponents.DotNetBar.Office2007Form
    {
        public frmExtractionData()
        {
            InitializeComponent();
        }

        private void frmExtractionData_Load(object sender, EventArgs e)
        {
            lblError.Visible = false;

            DisplayDept();

        }

        private void DisplayDept()
        {
            try
            {
                RefAdresseDAL refAdresseDAL = new RefAdresseDAL(Tools.DB.ConnexionName);

                DataTable deptDataSet = refAdresseDAL.GetDepartement();

                dpDept.DataSource = deptDataSet;
                dpDept.DisplayMember = "CodePostal";
                dpDept.ValueMember = "CodePostal";
                dpDept.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = ex.Message;
            }

        }


        private void picFolder_Click(object sender, EventArgs e)
        {
            string folderPath = "";
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                folderPath = folderBrowserDialog.SelectedPath;
                txtRepertoire.Text = folderPath;
            }
        }

        private void btnRefreshData_Click(object sender, EventArgs e)
        {
            try
            {
                if (dpDept.SelectedIndex != -1)
                {
                    lblError.Visible = false;
                    DisplayStats();
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = "Veuillez selectionner un code postal!";
                }

            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = ex.Message;
            }
        }

        private void DisplayStats()
        {
            try
            {
                InfoExtraitDAL refExtraitDAL = new InfoExtraitDAL(Tools.DB.ConnexionName);

                string cp = dpDept.Text.ToString();
                string typeAnnuaire = string.Empty;

                if (rdParticuliers.Checked)
                    typeAnnuaire = "PART";
                else if (rdProfessionnel.Checked)
                    typeAnnuaire = "PRO";

                DataTable statsDataSet = refExtraitDAL.GetNombreParCP(cp, typeAnnuaire);

                dgInfoCP.DataSource = statsDataSet;

            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = ex.Message;
            }

        }

        private void btnExtract_Click(object sender, EventArgs e)
        {
            btnExtract.Enabled = false;
            this.UseWaitCursor = true;

            try
            {
                lblError.Visible = false;
                lblError.Text = "";

                if (txtRepertoire.Text != string.Empty && txtNomFichier.Text != string.Empty)
                {
                    if (IsRowSelected())
                    {
                        ExtractInfoData();
                    }
                    else
                    {
                        lblError.Visible = true;
                        lblError.Text = "Veuillez s�lectionner le code postal et commune pour extrait le fichier!";
                    }

                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = "Veuillez saisir le nom du fichier et le repertoire!";
                }

            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = ex.Message;
            }

            btnExtract.Enabled = true;
            this.UseWaitCursor = false;
        }

        private bool IsRowSelected()
        {
            bool status = false;

            foreach (DataGridViewRow row in dgInfoCP.Rows)
            {
                DataGridViewCheckBoxCell chkStatsSelection = (DataGridViewCheckBoxCell)row.Cells["statsSelection"];

                if (chkStatsSelection != null && Convert.ToBoolean(chkStatsSelection.Value))
                {
                    status = true;
                    break;
                }
            }

            return status;
        }

        private void ExtractInfoData()
        {
            try
            {

                using (StreamWriter writer = new StreamWriter(File.Open((txtRepertoire.Text + "\\" + txtNomFichier.Text + ".txt"), FileMode.Create), Encoding.UTF8))
                {
                    if (rdParticuliers.Checked)
                        writer.WriteLine("Nom;Adresse;CP;Ville;Tel;Mobile");
                    else
                        writer.WriteLine("Soci�t�;Adresse;CP;Ville;Tel;Mobile;Type");

                    foreach (DataGridViewRow row in dgInfoCP.Rows)
                    {
                        DataGridViewCheckBoxCell chkStatsSelection = (DataGridViewCheckBoxCell)row.Cells["statsSelection"];

                        if (chkStatsSelection != null && Convert.ToBoolean(chkStatsSelection.Value))
                        {
                            DataGridViewTextBoxCell txtStatsCP = (DataGridViewTextBoxCell)row.Cells["statsCP"];
                            DataGridViewTextBoxCell txtStatsCommune = (DataGridViewTextBoxCell)row.Cells["statsCommune"];

                            string typeAnnuaire = string.Empty;

                            if (rdParticuliers.Checked)
                                typeAnnuaire = "PART";
                            else if (rdProfessionnel.Checked)
                                typeAnnuaire = "PRO";

                            InfoExtraitDAL infoExtraitDAL = new InfoExtraitDAL(Tools.DB.ConnexionName);
                            List<InfoExtrait> lesInfoExtrait = infoExtraitDAL.SelectParCP_Commune(typeAnnuaire, txtStatsCP.Value.ToString(), txtStatsCommune.Value.ToString());

                            foreach (InfoExtrait info in lesInfoExtrait)
                            {
                                if (rdParticuliers.Checked)
                                    writer.WriteLine(info.Nom + ";" + info.Adresse + ";" + info.CP + ";" + info.Ville + ";" + info.Tel + ";" + info.Mobile);
                                else
                                    writer.WriteLine(info.Nom + ";" + info.Adresse + ";" + info.CP + ";" + info.Ville + ";" + info.Tel + ";" + info.Mobile + ";" + info.TypeSociete);
                            }

                        }
                    }

                }

            }
            catch (Exception)
            {

                throw;
            }


        }


    }
}