﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using WebSpider.Utilities;

namespace Tools
{
    public class DB 
    {
        //static string _connectionName = @"Server=NB50502\SQLEXPRESS; Database=PagesJaunes; User ID=sa; Password=elca;";

        //static string _connectionName = @"Server=MAAHI-PC\SQLEXPRESS; Database=PagesJaunes; User ID=sa; Password=25012010;";

        //static string _connectionName = ConfigurationManager.ConnectionStrings["PAGESJAUNES"].ToString();

        static string _connectionName = @"Server=" + CRP.Decrypt("Uckwjv4sezIO7+sGM/LRwA==") + "; Database=" + CRP.Decrypt("lInjDra93uaoQOT8P5vlZQ==") + "; User ID=" + CRP.Decrypt("G6Ft2XL6p/OLxFNMxPp+5w==") + "; Password=" + CRP.Decrypt("nlKJ/oxOLBhhdV+Occ382w==") + ";";


        public static string ConnexionName
        {
            get
            {
                return _connectionName;
            }
            set
            {
                _connectionName = value;
            }
        }

        public static DateTime GetCurrentDateTime()
        {

            SqlConnection con = null;
            System.DateTime dateDuJour = DateTime.MinValue;

            try
            {
                con = new SqlConnection(ConnexionName);
                con.Open();
                SqlCommand cmd = new SqlCommand("SELECT GETDATE()", con);

                dateDuJour = System.DateTime.Parse(cmd.ExecuteScalar().ToString());
            }
            catch (SqlException sqlException)
            {
                throw sqlException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                    con.Dispose();
                }
            }
            return dateDuJour;
        }

        public static bool IsDatabaseAvailable()
        {
            SqlConnection con = null;
            bool isAvailable = false;

            try
            {
                con = new SqlConnection(ConnexionName);
                con.Open();
                isAvailable = true;
            }
            catch (SqlException sqlException)
            {
                throw sqlException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                    con.Dispose();
                }
            }
            return isAvailable;

        }

    }

}
