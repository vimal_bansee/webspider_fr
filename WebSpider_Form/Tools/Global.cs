﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Configuration;


namespace Tools
{
    public class Global
    {
        public static int MIN_INTERVAL = Convert.ToInt16(ConfigurationManager.AppSettings["MIN_INTERVAL"]);
        public static int MAX_INTERVAL = Convert.ToInt16(ConfigurationManager.AppSettings["MAX_INTERVAL"]);
        public static int RESTART_INTERVAL = Convert.ToInt16(ConfigurationManager.AppSettings["RESTART_INTERVAL"]);
        public static string EMAIL_TO = Convert.ToString(ConfigurationManager.AppSettings["EMAIL_TO"]);
    }

}
