﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using Tools;
using System.Security.Principal;
using System.Diagnostics;
using System.Reflection;
using System.Security.Permissions;

namespace WebSpider
{
    static class Program
    {

        static System.Threading.Timer MyTimer;

        static Thread MyThread;

        static frmExtractionWeb FormExtractionWeb;

        static int CommuneIndex = -1;

        /// The main entry point for the application.
        /*
        static void Main()
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new frmExtractionWeb());


            MyTimer = new System.Threading.Timer(new TimerCallback(OnCallBack), null, Global.RESTART_INTERVAL * 60000, Global.RESTART_INTERVAL * 60000);

            StartThread();

        }
        */

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmExtractionWeb());
            //Application.Run(new frmChrome());
        }


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        //[STAThread]
        static void MainTemp()
        {
            // Check if user is NOT admin
            if (!IsRunningAsAdministrator())
            {
                // Setting up start info of the new process of the same application
                ProcessStartInfo processStartInfo = new ProcessStartInfo(Assembly.GetEntryAssembly().CodeBase);

                // Using operating shell and setting the ProcessStartInfo.Verb to “runas” will let it run as admin
                processStartInfo.UseShellExecute = true;
                processStartInfo.Verb = "runas";

                //// Start the application as new process
                Process.Start(processStartInfo);

                //// Shut down the current (old) process
                System.Windows.Forms.Application.Exit();
            }

            //MyTimer = new System.Threading.Timer(new TimerCallback(OnCallBack), null, Global.RESTART_INTERVAL * 60000, Global.RESTART_INTERVAL * 60000);

            //StartThread();

        }

        /// <summary>
        /// Function that check's if current user is in Aministrator role
        /// </summary>
        /// <returns></returns>
        public static bool IsRunningAsAdministrator()
        {
            // Get current Windows user
            WindowsIdentity windowsIdentity = WindowsIdentity.GetCurrent();

            // Get current Windows user principal
            WindowsPrincipal windowsPrincipal = new WindowsPrincipal(windowsIdentity);

            // Return TRUE if user is in role "Administrator"
            return windowsPrincipal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        private static void OnCallBack(object obj)
        {
            try
            {
                //if (MyThread.IsAlive)
                //{
                //    FormExtractionWeb.Stop = true;

                //    //wait for the thread to terminate
                //    MyThread.Join(10000);

                //    //Abort the thread
                //    MyThread.Abort();                   
                //}

                while (MyThread.IsAlive)
                {
                    while (FormExtractionWeb.CanAbort == false)
                    {
                        FormExtractionWeb.Stop = true;

                        //wait for the thread to terminate for 3 mins
                        MyThread.Join(1800000);
                    }
                    //Abort the thread
                    MyThread.Abort();
                }

                if (FormExtractionWeb != null)
                    CommuneIndex = FormExtractionWeb.CommuneIndex;

                GC.Collect();
                GC.WaitForPendingFinalizers();

                StartThread();

            }
            catch (Exception)
            {

            }
            finally
            {
            }
        }

        static void StartThread()
        {
            try
            {
                MyThread = new Thread(LaunchForm);
                MyThread.ApartmentState = ApartmentState.STA;
                MyThread.Start();

                Application.Run();
            }
            catch (Exception)
            {

            }
            finally
            {
            }
        }

        static void LaunchForm()
        {
            try
            {
                FormExtractionWeb = new frmExtractionWeb();
                FormExtractionWeb.CommuneIndex = CommuneIndex;
                FormExtractionWeb.ShowDialog();
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

        }

    }
}
