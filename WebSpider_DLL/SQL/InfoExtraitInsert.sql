if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InfoExtraitInsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[InfoExtraitInsert]
GO

CREATE PROCEDURE [dbo].[InfoExtraitInsert]
(
	@TypeAnnuaire varchar(20),
	@Nom nvarchar(MAX),
	@Adresse nvarchar(MAX),
	@CP varchar(10),
	@Ville nvarchar(MAX),
	@Tel varchar(15),
	@Mobile varchar(15),
	@TypeSociete varchar(200),
	@RefAdresseId bigint,
	@DateModifier datetime
)

AS

SET NOCOUNT ON

INSERT INTO [dbo].[InfoExtrait]
(
	[TypeAnnuaire],
	[Nom],
	[Adresse],
	[CP],
	[Ville],
	[Tel],
	[Mobile],
	[TypeSociete],
	[RefAdresseId],
	[DateModifier]
)
VALUES
(
	@TypeAnnuaire,
	@Nom,
	@Adresse,
	@CP,
	@Ville,
	@Tel,
	@Mobile,
	@TypeSociete,
	@RefAdresseId,
	@DateModifier
)

SELECT SCOPE_IDENTITY()
GO
