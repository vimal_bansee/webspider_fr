if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RefAdresseDelete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[RefAdresseDelete]
GO

CREATE PROCEDURE [dbo].[RefAdresseDelete]
(
	@Id bigint
)

AS

SET NOCOUNT ON

DELETE FROM [dbo].[RefAdresse]
WHERE [Id] = @Id

SELECT  @@ROWCOUNT
GO
