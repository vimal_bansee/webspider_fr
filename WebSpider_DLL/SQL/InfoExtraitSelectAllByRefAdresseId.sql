if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InfoExtraitSelectAllByRefAdresseId]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[InfoExtraitSelectAllByRefAdresseId]
GO

CREATE PROCEDURE [dbo].[InfoExtraitSelectAllByRefAdresseId]
(
	@RefAdresseId bigint
)

AS

SET NOCOUNT ON

SELECT [Id],
	[TypeAnnuaire],
	[Nom],
	[Adresse],
	[CP],
	[Ville],
	[Tel],
	[Mobile],
	[TypeSociete],
	[RefAdresseId],
	[DateModifier]
FROM [dbo].[InfoExtrait]
WHERE [RefAdresseId] = @RefAdresseId
GO
