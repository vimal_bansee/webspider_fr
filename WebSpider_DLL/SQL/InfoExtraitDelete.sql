if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InfoExtraitDelete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[InfoExtraitDelete]
GO

CREATE PROCEDURE [dbo].[InfoExtraitDelete]
(
	@Id bigint
)

AS

SET NOCOUNT ON

DELETE FROM [dbo].[InfoExtrait]
WHERE [Id] = @Id

SELECT  @@ROWCOUNT
GO
