if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BanoDelete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[BanoDelete]
GO

CREATE PROCEDURE [dbo].[BanoDelete]
(
	@id varchar(50)
)

AS

SET NOCOUNT ON

DELETE FROM [dbo].[Bano]
WHERE [id] = @id

SELECT  @@ROWCOUNT
GO
