if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InfoExtraitUpdate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[InfoExtraitUpdate]
GO

CREATE PROCEDURE [dbo].[InfoExtraitUpdate]
(
	@Id bigint,
	@TypeAnnuaire varchar(20),
	@Nom nvarchar(MAX),
	@Adresse nvarchar(MAX),
	@CP varchar(10),
	@Ville nvarchar(MAX),
	@Tel varchar(15),
	@Mobile varchar(15),
	@TypeSociete varchar(200),
	@RefAdresseId bigint,
	@DateModifier datetime
)

AS

SET NOCOUNT ON

UPDATE [dbo].[InfoExtrait]
SET [TypeAnnuaire] = @TypeAnnuaire,
	[Nom] = @Nom,
	[Adresse] = @Adresse,
	[CP] = @CP,
	[Ville] = @Ville,
	[Tel] = @Tel,
	[Mobile] = @Mobile,
	[TypeSociete] = @TypeSociete,
	[RefAdresseId] = @RefAdresseId,
	[DateModifier] = @DateModifier
WHERE [Id] = @Id
SELECT  @@ROWCOUNT
GO
