if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InfoExtraitSelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[InfoExtraitSelectAll]
GO

CREATE PROCEDURE [dbo].[InfoExtraitSelectAll]

AS

SET NOCOUNT ON

SELECT [Id],
	[TypeAnnuaire],
	[Nom],
	[Adresse],
	[CP],
	[Ville],
	[Tel],
	[Mobile],
	[TypeSociete],
	[RefAdresseId],
	[DateModifier]
FROM [dbo].[InfoExtrait]
GO
