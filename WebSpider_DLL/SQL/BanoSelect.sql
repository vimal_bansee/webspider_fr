if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BanoSelect]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[BanoSelect]
GO

CREATE PROCEDURE [dbo].[BanoSelect]
(
	@id varchar(50)
)

AS

SET NOCOUNT ON

SELECT [id],
	[nom_voie],
	[id_fantoir],
	[numero],
	[rep],
	[code_insee],
	[code_post],
	[alias],
	[nom_ld],
	[x],
	[y],
	[commune],
	[fant_voie],
	[fant_ld],
	[lat],
	[lon]
FROM [dbo].[Bano]
WHERE [id] = @id
GO
