if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BanoSelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[BanoSelectAll]
GO

CREATE PROCEDURE [dbo].[BanoSelectAll]

AS

SET NOCOUNT ON

SELECT [id],
	[nom_voie],
	[id_fantoir],
	[numero],
	[rep],
	[code_insee],
	[code_post],
	[alias],
	[nom_ld],
	[x],
	[y],
	[commune],
	[fant_voie],
	[fant_ld],
	[lat],
	[lon]
FROM [dbo].[Bano]
GO
