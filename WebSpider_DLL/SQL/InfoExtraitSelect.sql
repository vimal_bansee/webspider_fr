if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InfoExtraitSelect]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[InfoExtraitSelect]
GO

CREATE PROCEDURE [dbo].[InfoExtraitSelect]
(
	@Id bigint
)

AS

SET NOCOUNT ON

SELECT [Id],
	[TypeAnnuaire],
	[Nom],
	[Adresse],
	[CP],
	[Ville],
	[Tel],
	[Mobile],
	[TypeSociete],
	[RefAdresseId],
	[DateModifier]
FROM [dbo].[InfoExtrait]
WHERE [Id] = @Id
GO
