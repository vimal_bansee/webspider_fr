if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RefAdresseSelect]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[RefAdresseSelect]
GO

CREATE PROCEDURE [dbo].[RefAdresseSelect]
(
	@Id bigint
)

AS

SET NOCOUNT ON

SELECT [Id],
	[Adresse],
	[CP],
	[Ville],
	[Pays],
	[Statut],
	[Erreur],
	[PageTotal],
	[PageExtrait],
	[DateModifier]
FROM [dbo].[RefAdresse]
WHERE [Id] = @Id
GO
