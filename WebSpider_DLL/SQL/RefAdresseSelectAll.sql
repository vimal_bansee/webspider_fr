if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RefAdresseSelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[RefAdresseSelectAll]
GO

CREATE PROCEDURE [dbo].[RefAdresseSelectAll]

AS

SET NOCOUNT ON

SELECT [Id],
	[Adresse],
	[CP],
	[Ville],
	[Pays],
	[Statut],
	[Erreur],
	[PageTotal],
	[PageExtrait],
	[DateModifier]
FROM [dbo].[RefAdresse]
GO
