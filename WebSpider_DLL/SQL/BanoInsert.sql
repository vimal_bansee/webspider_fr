if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BanoInsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[BanoInsert]
GO

CREATE PROCEDURE [dbo].[BanoInsert]
(
	@id varchar(50),
	@nom_voie varchar(50),
	@id_fantoir varchar(50),
	@numero varchar(50),
	@rep varchar(50),
	@code_insee varchar(50),
	@code_post varchar(50),
	@alias varchar(50),
	@nom_ld varchar(50),
	@x varchar(50),
	@y varchar(50),
	@commune varchar(50),
	@fant_voie varchar(50),
	@fant_ld varchar(50),
	@lat varchar(50),
	@lon varchar(50)
)

AS

SET NOCOUNT ON

INSERT INTO [dbo].[Bano]
(
	[id],
	[nom_voie],
	[id_fantoir],
	[numero],
	[rep],
	[code_insee],
	[code_post],
	[alias],
	[nom_ld],
	[x],
	[y],
	[commune],
	[fant_voie],
	[fant_ld],
	[lat],
	[lon]
)
VALUES
(
	@id,
	@nom_voie,
	@id_fantoir,
	@numero,
	@rep,
	@code_insee,
	@code_post,
	@alias,
	@nom_ld,
	@x,
	@y,
	@commune,
	@fant_voie,
	@fant_ld,
	@lat,
	@lon
)

SELECT  @@ROWCOUNT
GO
