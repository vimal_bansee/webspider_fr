if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BanoUpdate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[BanoUpdate]
GO

CREATE PROCEDURE [dbo].[BanoUpdate]
(
	@id varchar(50),
	@nom_voie varchar(50),
	@id_fantoir varchar(50),
	@numero varchar(50),
	@rep varchar(50),
	@code_insee varchar(50),
	@code_post varchar(50),
	@alias varchar(50),
	@nom_ld varchar(50),
	@x varchar(50),
	@y varchar(50),
	@commune varchar(50),
	@fant_voie varchar(50),
	@fant_ld varchar(50),
	@lat varchar(50),
	@lon varchar(50)
)

AS

SET NOCOUNT ON

UPDATE [dbo].[Bano]
SET [nom_voie] = @nom_voie,
	[id_fantoir] = @id_fantoir,
	[numero] = @numero,
	[rep] = @rep,
	[code_insee] = @code_insee,
	[code_post] = @code_post,
	[alias] = @alias,
	[nom_ld] = @nom_ld,
	[x] = @x,
	[y] = @y,
	[commune] = @commune,
	[fant_voie] = @fant_voie,
	[fant_ld] = @fant_ld,
	[lat] = @lat,
	[lon] = @lon
WHERE [id] = @id
SELECT  @@ROWCOUNT
GO
