if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RefAdresseUpdate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[RefAdresseUpdate]
GO

CREATE PROCEDURE [dbo].[RefAdresseUpdate]
(
	@Id bigint,
	@Adresse nvarchar(MAX),
	@CP nchar(10),
	@Ville nvarchar(250),
	@Pays nvarchar(200),
	@Statut int,
	@Erreur nvarchar(MAX),
	@PageTotal int,
	@PageExtrait int,
	@DateModifier datetime
)

AS

SET NOCOUNT ON

UPDATE [dbo].[RefAdresse]
SET [Adresse] = @Adresse,
	[CP] = @CP,
	[Ville] = @Ville,
	[Pays] = @Pays,
	[Statut] = @Statut,
	[Erreur] = @Erreur,
	[PageTotal] = @PageTotal,
	[PageExtrait] = @PageExtrait,
	[DateModifier] = @DateModifier
WHERE [Id] = @Id
SELECT  @@ROWCOUNT
GO
