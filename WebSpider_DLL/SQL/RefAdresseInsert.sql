if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RefAdresseInsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[RefAdresseInsert]
GO

CREATE PROCEDURE [dbo].[RefAdresseInsert]
(
	@Adresse nvarchar(MAX),
	@CP nchar(10),
	@Ville nvarchar(250),
	@Pays nvarchar(200),
	@Statut int,
	@Erreur nvarchar(MAX),
	@PageTotal int,
	@PageExtrait int,
	@DateModifier datetime
)

AS

SET NOCOUNT ON

INSERT INTO [dbo].[RefAdresse]
(
	[Adresse],
	[CP],
	[Ville],
	[Pays],
	[Statut],
	[Erreur],
	[PageTotal],
	[PageExtrait],
	[DateModifier]
)
VALUES
(
	@Adresse,
	@CP,
	@Ville,
	@Pays,
	@Statut,
	@Erreur,
	@PageTotal,
	@PageExtrait,
	@DateModifier
)

SELECT SCOPE_IDENTITY()
GO
