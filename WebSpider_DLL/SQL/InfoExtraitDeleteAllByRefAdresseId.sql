if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InfoExtraitDeleteAllByRefAdresseId]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[InfoExtraitDeleteAllByRefAdresseId]
GO

CREATE PROCEDURE [dbo].[InfoExtraitDeleteAllByRefAdresseId]
(
	@RefAdresseId bigint
)

AS

SET NOCOUNT ON

DELETE FROM [dbo].[InfoExtrait]
WHERE [RefAdresseId] = @RefAdresseId

SELECT  @@ROWCOUNT
GO
