using System;

namespace WebSpider_DLL
{
	public class InfoExtrait
	{
		#region Fields

		private long id;
		private string typeAnnuaire;
		private string nom;
		private string adresse;
		private string cP;
		private string ville;
		private string tel;
		private string mobile;
		private string typeSociete;
		private long refAdresseId;
		private DateTime dateModifier;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the InfoExtrait class.
		/// </summary>
		public InfoExtrait()
		{
		}

		/// <summary>
		/// Initializes a new instance of the InfoExtrait class.
		/// </summary>
		public InfoExtrait(string typeAnnuaire, string nom, string adresse, string cP, string ville, string tel, string mobile, string typeSociete, long refAdresseId, DateTime dateModifier)
		{
			this.typeAnnuaire = typeAnnuaire;
			this.nom = nom;
			this.adresse = adresse;
			this.cP = cP;
			this.ville = ville;
			this.tel = tel;
			this.mobile = mobile;
			this.typeSociete = typeSociete;
			this.refAdresseId = refAdresseId;
			this.dateModifier = dateModifier;
		}

		/// <summary>
		/// Initializes a new instance of the InfoExtrait class.
		/// </summary>
		public InfoExtrait(long id, string typeAnnuaire, string nom, string adresse, string cP, string ville, string tel, string mobile, string typeSociete, long refAdresseId, DateTime dateModifier)
		{
			this.id = id;
			this.typeAnnuaire = typeAnnuaire;
			this.nom = nom;
			this.adresse = adresse;
			this.cP = cP;
			this.ville = ville;
			this.tel = tel;
			this.mobile = mobile;
			this.typeSociete = typeSociete;
			this.refAdresseId = refAdresseId;
			this.dateModifier = dateModifier;
		}

		#endregion

		#region Properties
		/// <summary>
		/// Gets or sets the Id value.
		/// </summary>
		public virtual long Id
		{
			get { return id; }
			set { id = value; }
		}

		/// <summary>
		/// Gets or sets the TypeAnnuaire value.
		/// </summary>
		public virtual string TypeAnnuaire
		{
			get { return typeAnnuaire; }
			set { typeAnnuaire = value; }
		}

		/// <summary>
		/// Gets or sets the Nom value.
		/// </summary>
		public virtual string Nom
		{
			get { return nom; }
			set { nom = value; }
		}

		/// <summary>
		/// Gets or sets the Adresse value.
		/// </summary>
		public virtual string Adresse
		{
			get { return adresse; }
			set { adresse = value; }
		}

		/// <summary>
		/// Gets or sets the CP value.
		/// </summary>
		public virtual string CP
		{
			get { return cP; }
			set { cP = value; }
		}

		/// <summary>
		/// Gets or sets the Ville value.
		/// </summary>
		public virtual string Ville
		{
			get { return ville; }
			set { ville = value; }
		}

		/// <summary>
		/// Gets or sets the Tel value.
		/// </summary>
		public virtual string Tel
		{
			get { return tel; }
			set { tel = value; }
		}

		/// <summary>
		/// Gets or sets the Mobile value.
		/// </summary>
		public virtual string Mobile
		{
			get { return mobile; }
			set { mobile = value; }
		}

		/// <summary>
		/// Gets or sets the TypeSociete value.
		/// </summary>
		public virtual string TypeSociete
		{
			get { return typeSociete; }
			set { typeSociete = value; }
		}

		/// <summary>
		/// Gets or sets the RefAdresseId value.
		/// </summary>
		public virtual long RefAdresseId
		{
			get { return refAdresseId; }
			set { refAdresseId = value; }
		}

		/// <summary>
		/// Gets or sets the DateModifier value.
		/// </summary>
		public virtual DateTime DateModifier
		{
			get { return dateModifier; }
			set { dateModifier = value; }
		}

		#endregion
	}
}
