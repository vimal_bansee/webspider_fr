using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using SharpCore.Data;
using SharpCore.Utilities;

namespace WebSpider_DLL.DAL
{
    public partial class RefAdresseDAL
    {
        public enum ENUM_STATUS
        {
            AWAITING = 0,
            IN_PROGRESS = 1,
            TERMINATED = 2,
            FAILED = 3
        };

        public enum ENUM_TYPEANNUAIRE
        {
            PART,
            PRO    
        };

        /// <summary>
        /// Updates a record in the RefAdresse table.
        /// </summary>
        public virtual bool IsRefAdresseBusy(RefAdresse refAdresse)
        {
            string sqlCommand = ("UPDATE RefAdresse " +
                                 "SET    Statut = " + (int)ENUM_STATUS.IN_PROGRESS + ", DateModifier = getdate() " +
                                 "WHERE  (Statut =  " + (int)ENUM_STATUS.AWAITING + ") AND (Id = " + refAdresse.Id + ") " +
                                 "SELECT @@ROWCOUNT");

            int rowCount = (int)SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.Text, sqlCommand);

            if (rowCount == 0)
                return true;
            else
                return false;

        }

        /// <summary>
        /// Updates a record in the RefAdresse table.
        /// </summary>
        public virtual int GetNombreATraiter(string pCodePostal, string pVille)
        {
            string sqlCommand = ("SELECT COUNT(*) AS Total " +
                                 "FROM RefAdresse " +
                                 "WHERE (Statut = " + (int)ENUM_STATUS.AWAITING + ") AND (LTRIM(RTRIM(CP)) = '" + pCodePostal.Trim() + "') AND (LTRIM(RTRIM(Ville)) = '" + pVille.Trim() + "')");

            int nombre = (int)SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.Text, sqlCommand);

            return nombre;

        }
        

        /// <summary>
        /// Updates a record in the RefAdresse table.
        /// </summary>
        public virtual int GetNombreATraiter(string pCodePostal, string pVille, RefAdresseDAL.ENUM_TYPEANNUAIRE pTypeAddress)
        {
            string sqlCommand = ("SELECT COUNT(*) AS Total " +
                                 "FROM RefAdresse " +
                                 "WHERE (Statut = " + (int)ENUM_STATUS.AWAITING + ") AND " +
                                 "      (LTRIM(RTRIM(CP)) = '" + pCodePostal.Trim() + "') AND " +
                                 "      (LTRIM(RTRIM(Ville)) = '" + pVille.Trim() + "')  AND " +
                                 "      (TypeAnnuaire = '" + pTypeAddress.ToString() + "')"
                                );

            int nombre = (int)SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.Text, sqlCommand);

            return nombre;

        }


        /// Updates a record in the RefAdresse table.
        /// </summary>
        public virtual bool ResetFichiersEnEchec(string pCodePostal, string pVille, string pTypeAnnuaire, bool pResetAll)
        {
            string sqlCommand = ("UPDATE RefAdresse " +
                                 "SET    Statut = " + (int)ENUM_STATUS.AWAITING + ", DateModifier = getdate() " +
                                 "WHERE  (Statut =  " + (int)ENUM_STATUS.FAILED + " OR Statut =  " + (int)ENUM_STATUS.IN_PROGRESS + ") AND (LTRIM(RTRIM(CP)) = '" + pCodePostal.Trim() + "') AND (LTRIM(RTRIM(Ville)) = '" + pVille.Trim() + "') AND (TypeAnnuaire = '" + pTypeAnnuaire + "') " +
                                 "SELECT @@ROWCOUNT");

            if(pResetAll)
            {
                sqlCommand = ("UPDATE RefAdresse " +
                                 "SET    Statut = " + (int)ENUM_STATUS.AWAITING + ", PageExtrait = 0 , DateModifier = getdate() " +
                                 "WHERE  (LTRIM(RTRIM(CP)) = '" + pCodePostal.Trim() + "') AND (LTRIM(RTRIM(Ville)) = '" + pVille.Trim() + "') AND (TypeAnnuaire = '" + pTypeAnnuaire + "') " +
                                 "SELECT @@ROWCOUNT");
            }

            int rowCount = (int)SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.Text, sqlCommand);

            if (rowCount == 0)
                return false;
            else
                return true;

        }


        public DataTable GetCPCommune()
        {
            DataTable deptDataTable = null;

            try
            {
                string sqlCommand = "SELECT LTRIM(RTRIM(CP)) AS CodePostal, LTRIM(RTRIM(Ville)) AS Commune, LTRIM(RTRIM(CP)) + N' - ' + LTRIM(RTRIM(Ville)) AS Adresse " +
                                    "FROM   dbo.RefAdresse " +
                                    "GROUP BY LTRIM(RTRIM(CP)), LTRIM(RTRIM(Ville)) " +
                                    "ORDER BY CodePostal, Commune";

                deptDataTable = SqlClientUtility.ExecuteDataTable(connectionStringName, CommandType.Text, sqlCommand);

            }
            catch (Exception)
            {

                throw;
            }


            return deptDataTable;
        }

        public DataTable GetDepartement()
        {
            DataTable deptDataTable = null;

            try
            {
                string sqlCommand = "SELECT LEFT(LTRIM(RTRIM(CP)), 2) AS CodePostal " +
                                    "FROM   dbo.RefAdresse " +
                                    "GROUP BY LEFT(LTRIM(RTRIM(CP)), 2) " +
                                    "ORDER BY LEFT(LTRIM(RTRIM(CP)), 2)";

                deptDataTable = SqlClientUtility.ExecuteDataTable(connectionStringName, CommandType.Text, sqlCommand);

            }
            catch (Exception)
            {

                throw;
            }


            return deptDataTable;
        }


    }
}
