using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using SharpCore.Data;
using SharpCore.Utilities;

namespace WebSpider_DLL.DAL
{
	public partial class InfoExtraitDAL
	{
        public DataTable GetNombreParCP(string pCP, string pTypeAnnuaire)
        {
            DataTable deptDataTable = null;

            try
            {
                string sqlCommand = "SELECT CP, Ville AS Commune, COUNT(*) AS Nombre " +
                                    "FROM  (SELECT DISTINCT TypeAnnuaire, Nom, Adresse, CP, Ville, Tel, Mobile, TypeSociete " +
                                    "       FROM    dbo.InfoExtrait " +
                                    "       WHERE   (TypeAnnuaire = '" + pTypeAnnuaire + "') AND (LEFT(LTRIM(RTRIM(CP)), 2) = '" + pCP + "') " +
                                    "      ) AS GroupInfoExtrait " +
                                    "GROUP BY CP, Ville " +
                                    "ORDER BY CP, Commune";

                deptDataTable = SqlClientUtility.ExecuteDataTable(connectionStringName, CommandType.Text, sqlCommand);

            }
            catch (Exception)
            {

                throw;
            }


            return deptDataTable;
        }

        /// <summary>
        /// Selects a single record from the InfoExtrait table.
        /// </summary>
        public virtual List<InfoExtrait> SelectParCP_Commune(string pTypeAnnuaire, string pCP, string pCommune)
        {
            string sqlCommand = "SELECT TypeAnnuaire, Nom, Adresse, CP, Ville, Tel, Mobile, TypeSociete, MAX(RefAdresseId) AS RefAdresseId, MAX(DateModifier) AS DateModifier, MAX(Id) AS Id " +
                                "FROM dbo.InfoExtrait " +
                                "WHERE (TypeAnnuaire = '" + pTypeAnnuaire + "') AND (LTRIM(RTRIM(CP)) = '" + pCP.Trim() + "') AND (LTRIM(RTRIM(Ville)) = '" + pCommune.Trim() + "') " +
                                "GROUP BY TypeAnnuaire, Nom, Adresse, CP, Ville, Tel, Mobile, TypeSociete";

            //string sqlCommand = "SELECT DISTINCT TypeAnnuaire, Nom, Adresse, CP, Ville, Tel, Mobile, TypeSociete " +
            //        "FROM dbo.InfoExtrait " +
            //        "WHERE (TypeAnnuaire = '" + pTypeAnnuaire + "') AND (LTRIM(RTRIM(CP)) = '" + pCP.Trim() + "') AND (LTRIM(RTRIM(Ville)) = '" + pCommune.Trim() + "') ";

            using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.Text, sqlCommand))
            {
				List<InfoExtrait> infoExtraitList = new List<InfoExtrait>();
				while (dataReader.Read())
				{
					InfoExtrait infoExtrait = MakeInfoExtrait(dataReader);
					infoExtraitList.Add(infoExtrait);
				}

				SqlClientUtility.CloseConnection();
				return infoExtraitList;
			}
        }

        public virtual int GetNombreTotalParCP(string pCP, string pCommune)
        {
            string sqlCommand = ("SELECT COUNT(Id) AS Nombre " +
                                 "FROM  InfoExtrait " +
                                 "WHERE     (LTRIM(RTRIM(CP)) = '" + pCP + "') AND (LTRIM(RTRIM(Ville)) = '" + pCommune + "')");

            int nombre = (int)SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.Text, sqlCommand);

            return nombre;

        }

	}
}
