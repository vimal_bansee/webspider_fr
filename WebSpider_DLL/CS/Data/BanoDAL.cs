using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using SharpCore.Data;
using SharpCore.Utilities;

namespace WebSpider_DLL.DAL
{
	public partial class BanoDAL
	{
		#region Fields

		protected string connectionStringName;

		#endregion

		#region Constructors

		public BanoDAL(string connectionStringName)
		{
			ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

			this.connectionStringName = connectionStringName;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Saves a record to the Bano table.
		/// </summary>
		public virtual object Insert(Bano bano)
		{
			ValidationUtility.ValidateArgument("bano", bano);

			SqlParameter[] parameters = new SqlParameter[]
			{
				new SqlParameter("@id", bano.Id),
				new SqlParameter("@nom_voie", bano.Nom_voie),
				new SqlParameter("@id_fantoir", bano.Id_fantoir),
				new SqlParameter("@numero", bano.Numero),
				new SqlParameter("@rep", bano.Rep),
				new SqlParameter("@code_insee", bano.Code_insee),
				new SqlParameter("@code_post", bano.Code_post),
				new SqlParameter("@alias", bano.Alias),
				new SqlParameter("@nom_ld", bano.Nom_ld),
				new SqlParameter("@x", bano.X),
				new SqlParameter("@y", bano.Y),
				new SqlParameter("@commune", bano.Commune),
				new SqlParameter("@fant_voie", bano.Fant_voie),
				new SqlParameter("@fant_ld", bano.Fant_ld),
				new SqlParameter("@lat", bano.Lat),
				new SqlParameter("@lon", bano.Lon)
			};

			return (object) SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "dbo.BanoInsert", parameters);
		}

		/// <summary>
		/// Updates a record in the Bano table.
		/// </summary>
		public virtual int Update(Bano bano)
		{
			ValidationUtility.ValidateArgument("bano", bano);

			SqlParameter[] parameters = new SqlParameter[]
			{
				new SqlParameter("@id", bano.Id),
				new SqlParameter("@nom_voie", bano.Nom_voie),
				new SqlParameter("@id_fantoir", bano.Id_fantoir),
				new SqlParameter("@numero", bano.Numero),
				new SqlParameter("@rep", bano.Rep),
				new SqlParameter("@code_insee", bano.Code_insee),
				new SqlParameter("@code_post", bano.Code_post),
				new SqlParameter("@alias", bano.Alias),
				new SqlParameter("@nom_ld", bano.Nom_ld),
				new SqlParameter("@x", bano.X),
				new SqlParameter("@y", bano.Y),
				new SqlParameter("@commune", bano.Commune),
				new SqlParameter("@fant_voie", bano.Fant_voie),
				new SqlParameter("@fant_ld", bano.Fant_ld),
				new SqlParameter("@lat", bano.Lat),
				new SqlParameter("@lon", bano.Lon)
			};

			return (int) SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "dbo.BanoUpdate", parameters);
		}

		/// <summary>
		/// Deletes a record from the Bano table by its primary key.
		/// </summary>
		public virtual int Delete(string id)
		{
			SqlParameter[] parameters = new SqlParameter[]
			{
				new SqlParameter("@id", id)
			};

			return (int) SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "dbo.BanoDelete", parameters);
		}

		/// <summary>
		/// Selects a single record from the Bano table.
		/// </summary>
		public virtual Bano Select(string id)
		{
			SqlParameter[] parameters = new SqlParameter[]
			{
				new SqlParameter("@id", id)
			};

			using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "dbo.BanoSelect", parameters))
			{

				Bano bano = null;
				if (dataReader.Read())
				{
					bano = MakeBano(dataReader);
				}

				SqlClientUtility.CloseConnection();
				return bano;
			}
		}

		/// <summary>
		/// Selects all records from the Bano table.
		/// </summary>
		public virtual List<Bano> SelectAll()
		{
			using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "dbo.BanoSelectAll"))
			{
				List<Bano> banoList = new List<Bano>();
				while (dataReader.Read())
				{
					Bano bano = MakeBano(dataReader);
					banoList.Add(bano);
				}

				SqlClientUtility.CloseConnection();
				return banoList;
			}
		}

		/// <summary>
		/// Creates a new instance of the Bano class and populates it with data from the specified SqlDataReader.
		/// </summary>
		protected virtual Bano MakeBano(SqlDataReader dataReader)
		{
			Bano bano = new Bano();
			bano.Id = SqlClientUtility.GetString(dataReader, "id", String.Empty);
			bano.Nom_voie = SqlClientUtility.GetString(dataReader, "nom_voie", String.Empty);
			bano.Id_fantoir = SqlClientUtility.GetString(dataReader, "id_fantoir", String.Empty);
			bano.Numero = SqlClientUtility.GetString(dataReader, "numero", String.Empty);
			bano.Rep = SqlClientUtility.GetString(dataReader, "rep", String.Empty);
			bano.Code_insee = SqlClientUtility.GetString(dataReader, "code_insee", String.Empty);
			bano.Code_post = SqlClientUtility.GetString(dataReader, "code_post", String.Empty);
			bano.Alias = SqlClientUtility.GetString(dataReader, "alias", String.Empty);
			bano.Nom_ld = SqlClientUtility.GetString(dataReader, "nom_ld", String.Empty);
			bano.X = SqlClientUtility.GetString(dataReader, "x", String.Empty);
			bano.Y = SqlClientUtility.GetString(dataReader, "y", String.Empty);
			bano.Commune = SqlClientUtility.GetString(dataReader, "commune", String.Empty);
			bano.Fant_voie = SqlClientUtility.GetString(dataReader, "fant_voie", String.Empty);
			bano.Fant_ld = SqlClientUtility.GetString(dataReader, "fant_ld", String.Empty);
			bano.Lat = SqlClientUtility.GetString(dataReader, "lat", String.Empty);
			bano.Lon = SqlClientUtility.GetString(dataReader, "lon", String.Empty);

			return bano;
		}

		#endregion
	}
}
