using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using SharpCore.Data;
using SharpCore.Utilities;

namespace WebSpider_DLL.DAL
{
	public partial class InfoExtraitDAL
	{
		#region Fields

		protected string connectionStringName;

		#endregion

		#region Constructors

		public InfoExtraitDAL(string connectionStringName)
		{
			ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

			this.connectionStringName = connectionStringName;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Saves a record to the InfoExtrait table.
		/// </summary>
		public virtual object Insert(InfoExtrait infoExtrait)
		{
			ValidationUtility.ValidateArgument("infoExtrait", infoExtrait);

			SqlParameter[] parameters = new SqlParameter[]
			{
				new SqlParameter("@TypeAnnuaire", infoExtrait.TypeAnnuaire),
				new SqlParameter("@Nom", infoExtrait.Nom),
				new SqlParameter("@Adresse", infoExtrait.Adresse),
				new SqlParameter("@CP", infoExtrait.CP),
				new SqlParameter("@Ville", infoExtrait.Ville),
				new SqlParameter("@Tel", infoExtrait.Tel),
				new SqlParameter("@Mobile", infoExtrait.Mobile),
				new SqlParameter("@TypeSociete", infoExtrait.TypeSociete),
				new SqlParameter("@RefAdresseId", infoExtrait.RefAdresseId),
				new SqlParameter("@DateModifier", infoExtrait.DateModifier)
			};

			return (object) SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "dbo.InfoExtraitInsert", parameters);
		}

		/// <summary>
		/// Updates a record in the InfoExtrait table.
		/// </summary>
		public virtual int Update(InfoExtrait infoExtrait)
		{
			ValidationUtility.ValidateArgument("infoExtrait", infoExtrait);

			SqlParameter[] parameters = new SqlParameter[]
			{
				new SqlParameter("@Id", infoExtrait.Id),
				new SqlParameter("@TypeAnnuaire", infoExtrait.TypeAnnuaire),
				new SqlParameter("@Nom", infoExtrait.Nom),
				new SqlParameter("@Adresse", infoExtrait.Adresse),
				new SqlParameter("@CP", infoExtrait.CP),
				new SqlParameter("@Ville", infoExtrait.Ville),
				new SqlParameter("@Tel", infoExtrait.Tel),
				new SqlParameter("@Mobile", infoExtrait.Mobile),
				new SqlParameter("@TypeSociete", infoExtrait.TypeSociete),
				new SqlParameter("@RefAdresseId", infoExtrait.RefAdresseId),
				new SqlParameter("@DateModifier", infoExtrait.DateModifier)
			};

			return (int) SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "dbo.InfoExtraitUpdate", parameters);
		}

		/// <summary>
		/// Deletes a record from the InfoExtrait table by its primary key.
		/// </summary>
		public virtual int Delete(long id)
		{
			SqlParameter[] parameters = new SqlParameter[]
			{
				new SqlParameter("@Id", id)
			};

			return (int) SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "dbo.InfoExtraitDelete", parameters);
		}

		/// <summary>
		/// Deletes a record from the InfoExtrait table by a foreign key.
		/// </summary>
		public virtual int DeleteAllByRefAdresseId(long refAdresseId)
		{
			SqlParameter[] parameters = new SqlParameter[]
			{
				new SqlParameter("@RefAdresseId", refAdresseId)
			};

			return (int) SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "dbo.InfoExtraitDeleteAllByRefAdresseId", parameters);
		}

		/// <summary>
		/// Selects a single record from the InfoExtrait table.
		/// </summary>
		public virtual InfoExtrait Select(long id)
		{
			SqlParameter[] parameters = new SqlParameter[]
			{
				new SqlParameter("@Id", id)
			};

			using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "dbo.InfoExtraitSelect", parameters))
			{

				InfoExtrait infoExtrait = null;
				if (dataReader.Read())
				{
					infoExtrait = MakeInfoExtrait(dataReader);
				}

				SqlClientUtility.CloseConnection();
				return infoExtrait;
			}
		}

		/// <summary>
		/// Selects all records from the InfoExtrait table.
		/// </summary>
		public virtual List<InfoExtrait> SelectAll()
		{
			using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "dbo.InfoExtraitSelectAll"))
			{
				List<InfoExtrait> infoExtraitList = new List<InfoExtrait>();
				while (dataReader.Read())
				{
					InfoExtrait infoExtrait = MakeInfoExtrait(dataReader);
					infoExtraitList.Add(infoExtrait);
				}

				SqlClientUtility.CloseConnection();
				return infoExtraitList;
			}
		}

		/// <summary>
		/// Selects all records from the InfoExtrait table by a foreign key.
		/// </summary>
		public virtual List<InfoExtrait> SelectAllByRefAdresseId(long refAdresseId)
		{
			SqlParameter[] parameters = new SqlParameter[]
			{
				new SqlParameter("@RefAdresseId", refAdresseId)
			};

			using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "dbo.InfoExtraitSelectAllByRefAdresseId", parameters))
			{
				List<InfoExtrait> infoExtraitList = new List<InfoExtrait>();
				while (dataReader.Read())
				{
					InfoExtrait infoExtrait = MakeInfoExtrait(dataReader);
					infoExtraitList.Add(infoExtrait);
				}

				SqlClientUtility.CloseConnection();
				return infoExtraitList;
			}
		}

		/// <summary>
		/// Creates a new instance of the InfoExtrait class and populates it with data from the specified SqlDataReader.
		/// </summary>
		protected virtual InfoExtrait MakeInfoExtrait(SqlDataReader dataReader)
		{
			InfoExtrait infoExtrait = new InfoExtrait();
			infoExtrait.Id = SqlClientUtility.GetInt64(dataReader, "Id", 0);
			infoExtrait.TypeAnnuaire = SqlClientUtility.GetString(dataReader, "TypeAnnuaire", String.Empty);
			infoExtrait.Nom = SqlClientUtility.GetString(dataReader, "Nom", String.Empty);
			infoExtrait.Adresse = SqlClientUtility.GetString(dataReader, "Adresse", String.Empty);
			infoExtrait.CP = SqlClientUtility.GetString(dataReader, "CP", String.Empty);
			infoExtrait.Ville = SqlClientUtility.GetString(dataReader, "Ville", String.Empty);
			infoExtrait.Tel = SqlClientUtility.GetString(dataReader, "Tel", String.Empty);
			infoExtrait.Mobile = SqlClientUtility.GetString(dataReader, "Mobile", String.Empty);
			infoExtrait.TypeSociete = SqlClientUtility.GetString(dataReader, "TypeSociete", String.Empty);
			infoExtrait.RefAdresseId = SqlClientUtility.GetInt64(dataReader, "RefAdresseId", 0);
			infoExtrait.DateModifier = SqlClientUtility.GetDateTime(dataReader, "DateModifier", DateTime.Now);

			return infoExtrait;
		}

		#endregion
	}
}
