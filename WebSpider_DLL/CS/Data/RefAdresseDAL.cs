using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using SharpCore.Data;
using SharpCore.Utilities;

namespace WebSpider_DLL.DAL
{
	public partial class RefAdresseDAL
	{
		#region Fields

		protected string connectionStringName;

		#endregion

		#region Constructors

		public RefAdresseDAL(string connectionStringName)
		{
			ValidationUtility.ValidateArgument("connectionStringName", connectionStringName);

			this.connectionStringName = connectionStringName;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Saves a record to the RefAdresse table.
		/// </summary>
		public virtual object Insert(RefAdresse refAdresse)
		{
			ValidationUtility.ValidateArgument("refAdresse", refAdresse);

			SqlParameter[] parameters = new SqlParameter[]
			{
				new SqlParameter("@Adresse", refAdresse.Adresse),
				new SqlParameter("@CP", refAdresse.CP),
				new SqlParameter("@Ville", refAdresse.Ville),
				new SqlParameter("@Pays", refAdresse.Pays),
				new SqlParameter("@Statut", refAdresse.Statut),
				new SqlParameter("@Erreur", refAdresse.Erreur),
				new SqlParameter("@PageTotal", refAdresse.PageTotal),
				new SqlParameter("@PageExtrait", refAdresse.PageExtrait),
				new SqlParameter("@DateModifier", refAdresse.DateModifier)
			};

			return (object) SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "dbo.RefAdresseInsert", parameters);
		}

		/// <summary>
		/// Updates a record in the RefAdresse table.
		/// </summary>
		public virtual int Update(RefAdresse refAdresse)
		{
			ValidationUtility.ValidateArgument("refAdresse", refAdresse);

			SqlParameter[] parameters = new SqlParameter[]
			{
				new SqlParameter("@Id", refAdresse.Id),
				new SqlParameter("@Adresse", refAdresse.Adresse),
				new SqlParameter("@CP", refAdresse.CP),
				new SqlParameter("@Ville", refAdresse.Ville),
				new SqlParameter("@Pays", refAdresse.Pays),
				new SqlParameter("@Statut", refAdresse.Statut),
				new SqlParameter("@Erreur", refAdresse.Erreur),
				new SqlParameter("@PageTotal", refAdresse.PageTotal),
				new SqlParameter("@PageExtrait", refAdresse.PageExtrait),
				new SqlParameter("@DateModifier", refAdresse.DateModifier)
			};

			return (int) SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "dbo.RefAdresseUpdate", parameters);
		}

		/// <summary>
		/// Deletes a record from the RefAdresse table by its primary key.
		/// </summary>
		public virtual int Delete(long id)
		{
			SqlParameter[] parameters = new SqlParameter[]
			{
				new SqlParameter("@Id", id)
			};

			return (int) SqlClientUtility.ExecuteScalar(connectionStringName, CommandType.StoredProcedure, "dbo.RefAdresseDelete", parameters);
		}

		/// <summary>
		/// Selects a single record from the RefAdresse table.
		/// </summary>
		public virtual RefAdresse Select(long id)
		{
			SqlParameter[] parameters = new SqlParameter[]
			{
				new SqlParameter("@Id", id)
			};

			using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "dbo.RefAdresseSelect", parameters))
			{

				RefAdresse refAdresse = null;
				if (dataReader.Read())
				{
					refAdresse = MakeRefAdresse(dataReader);
				}

				SqlClientUtility.CloseConnection();
				return refAdresse;
			}
		}

		/// <summary>
		/// Selects all records from the RefAdresse table.
		/// </summary>
		public virtual List<RefAdresse> SelectAll()
		{
			using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "dbo.RefAdresseSelectAll"))
			{
				List<RefAdresse> refAdresseList = new List<RefAdresse>();
				while (dataReader.Read())
				{
					RefAdresse refAdresse = MakeRefAdresse(dataReader);
					refAdresseList.Add(refAdresse);
				}

				SqlClientUtility.CloseConnection();
				return refAdresseList;
			}
		}

		/// <summary>
		/// Creates a new instance of the RefAdresse class and populates it with data from the specified SqlDataReader.
		/// </summary>
		public virtual RefAdresse MakeRefAdresse(SqlDataReader dataReader)
		{
			RefAdresse refAdresse = new RefAdresse();
			refAdresse.Id = SqlClientUtility.GetInt64(dataReader, "Id", 0);
			refAdresse.Adresse = SqlClientUtility.GetString(dataReader, "Adresse", String.Empty);
			refAdresse.CP = SqlClientUtility.GetString(dataReader, "CP", String.Empty);
			refAdresse.Ville = SqlClientUtility.GetString(dataReader, "Ville", String.Empty);
			refAdresse.Pays = SqlClientUtility.GetString(dataReader, "Pays", String.Empty);
			refAdresse.Statut = SqlClientUtility.GetInt32(dataReader, "Statut", 0);
			refAdresse.Erreur = SqlClientUtility.GetString(dataReader, "Erreur", String.Empty);
			refAdresse.PageTotal = SqlClientUtility.GetInt32(dataReader, "PageTotal", 0);
			refAdresse.PageExtrait = SqlClientUtility.GetInt32(dataReader, "PageExtrait", 0);
			refAdresse.DateModifier = SqlClientUtility.GetDateTime(dataReader, "DateModifier", DateTime.Now);

			return refAdresse;
		}

		#endregion
	}
}
