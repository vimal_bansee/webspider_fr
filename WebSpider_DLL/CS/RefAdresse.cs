using System;

namespace WebSpider_DLL
{
	public class RefAdresse
	{
		#region Fields

		private long id;
		private string adresse;
		private string cP;
		private string ville;
		private string pays;
		private int statut;
		private string erreur;
		private int pageTotal;
		private int pageExtrait;
		private DateTime dateModifier;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RefAdresse class.
		/// </summary>
		public RefAdresse()
		{
		}

		/// <summary>
		/// Initializes a new instance of the RefAdresse class.
		/// </summary>
		public RefAdresse(string adresse, string cP, string ville, string pays, int statut, string erreur, int pageTotal, int pageExtrait, DateTime dateModifier)
		{
			this.adresse = adresse;
			this.cP = cP;
			this.ville = ville;
			this.pays = pays;
			this.statut = statut;
			this.erreur = erreur;
			this.pageTotal = pageTotal;
			this.pageExtrait = pageExtrait;
			this.dateModifier = dateModifier;
		}

		/// <summary>
		/// Initializes a new instance of the RefAdresse class.
		/// </summary>
		public RefAdresse(long id, string adresse, string cP, string ville, string pays, int statut, string erreur, int pageTotal, int pageExtrait, DateTime dateModifier)
		{
			this.id = id;
			this.adresse = adresse;
			this.cP = cP;
			this.ville = ville;
			this.pays = pays;
			this.statut = statut;
			this.erreur = erreur;
			this.pageTotal = pageTotal;
			this.pageExtrait = pageExtrait;
			this.dateModifier = dateModifier;
		}

		#endregion

		#region Properties
		/// <summary>
		/// Gets or sets the Id value.
		/// </summary>
		public virtual long Id
		{
			get { return id; }
			set { id = value; }
		}

		/// <summary>
		/// Gets or sets the Adresse value.
		/// </summary>
		public virtual string Adresse
		{
			get { return adresse; }
			set { adresse = value; }
		}

		/// <summary>
		/// Gets or sets the CP value.
		/// </summary>
		public virtual string CP
		{
			get { return cP; }
			set { cP = value; }
		}

		/// <summary>
		/// Gets or sets the Ville value.
		/// </summary>
		public virtual string Ville
		{
			get { return ville; }
			set { ville = value; }
		}

		/// <summary>
		/// Gets or sets the Pays value.
		/// </summary>
		public virtual string Pays
		{
			get { return pays; }
			set { pays = value; }
		}

		/// <summary>
		/// Gets or sets the Statut value.
		/// </summary>
		public virtual int Statut
		{
			get { return statut; }
			set { statut = value; }
		}

		/// <summary>
		/// Gets or sets the Erreur value.
		/// </summary>
		public virtual string Erreur
		{
			get { return erreur; }
			set { erreur = value; }
		}

		/// <summary>
		/// Gets or sets the PageTotal value.
		/// </summary>
		public virtual int PageTotal
		{
			get { return pageTotal; }
			set { pageTotal = value; }
		}

		/// <summary>
		/// Gets or sets the PageExtrait value.
		/// </summary>
		public virtual int PageExtrait
		{
			get { return pageExtrait; }
			set { pageExtrait = value; }
		}

		/// <summary>
		/// Gets or sets the DateModifier value.
		/// </summary>
		public virtual DateTime DateModifier
		{
			get { return dateModifier; }
			set { dateModifier = value; }
		}

		#endregion
	}
}
