using System;

namespace WebSpider_DLL
{
	public class Bano
	{
		#region Fields

		private string id;
		private string nom_voie;
		private string id_fantoir;
		private string numero;
		private string rep;
		private string code_insee;
		private string code_post;
		private string alias;
		private string nom_ld;
		private string x;
		private string y;
		private string commune;
		private string fant_voie;
		private string fant_ld;
		private string lat;
		private string lon;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the Bano class.
		/// </summary>
		public Bano()
		{
		}

		/// <summary>
		/// Initializes a new instance of the Bano class.
		/// </summary>
		public Bano(string id, string nom_voie, string id_fantoir, string numero, string rep, string code_insee, string code_post, string alias, string nom_ld, string x, string y, string commune, string fant_voie, string fant_ld, string lat, string lon)
		{
			this.id = id;
			this.nom_voie = nom_voie;
			this.id_fantoir = id_fantoir;
			this.numero = numero;
			this.rep = rep;
			this.code_insee = code_insee;
			this.code_post = code_post;
			this.alias = alias;
			this.nom_ld = nom_ld;
			this.x = x;
			this.y = y;
			this.commune = commune;
			this.fant_voie = fant_voie;
			this.fant_ld = fant_ld;
			this.lat = lat;
			this.lon = lon;
		}

		#endregion

		#region Properties
		/// <summary>
		/// Gets or sets the Id value.
		/// </summary>
		public virtual string Id
		{
			get { return id; }
			set { id = value; }
		}

		/// <summary>
		/// Gets or sets the Nom_voie value.
		/// </summary>
		public virtual string Nom_voie
		{
			get { return nom_voie; }
			set { nom_voie = value; }
		}

		/// <summary>
		/// Gets or sets the Id_fantoir value.
		/// </summary>
		public virtual string Id_fantoir
		{
			get { return id_fantoir; }
			set { id_fantoir = value; }
		}

		/// <summary>
		/// Gets or sets the Numero value.
		/// </summary>
		public virtual string Numero
		{
			get { return numero; }
			set { numero = value; }
		}

		/// <summary>
		/// Gets or sets the Rep value.
		/// </summary>
		public virtual string Rep
		{
			get { return rep; }
			set { rep = value; }
		}

		/// <summary>
		/// Gets or sets the Code_insee value.
		/// </summary>
		public virtual string Code_insee
		{
			get { return code_insee; }
			set { code_insee = value; }
		}

		/// <summary>
		/// Gets or sets the Code_post value.
		/// </summary>
		public virtual string Code_post
		{
			get { return code_post; }
			set { code_post = value; }
		}

		/// <summary>
		/// Gets or sets the Alias value.
		/// </summary>
		public virtual string Alias
		{
			get { return alias; }
			set { alias = value; }
		}

		/// <summary>
		/// Gets or sets the Nom_ld value.
		/// </summary>
		public virtual string Nom_ld
		{
			get { return nom_ld; }
			set { nom_ld = value; }
		}

		/// <summary>
		/// Gets or sets the X value.
		/// </summary>
		public virtual string X
		{
			get { return x; }
			set { x = value; }
		}

		/// <summary>
		/// Gets or sets the Y value.
		/// </summary>
		public virtual string Y
		{
			get { return y; }
			set { y = value; }
		}

		/// <summary>
		/// Gets or sets the Commune value.
		/// </summary>
		public virtual string Commune
		{
			get { return commune; }
			set { commune = value; }
		}

		/// <summary>
		/// Gets or sets the Fant_voie value.
		/// </summary>
		public virtual string Fant_voie
		{
			get { return fant_voie; }
			set { fant_voie = value; }
		}

		/// <summary>
		/// Gets or sets the Fant_ld value.
		/// </summary>
		public virtual string Fant_ld
		{
			get { return fant_ld; }
			set { fant_ld = value; }
		}

		/// <summary>
		/// Gets or sets the Lat value.
		/// </summary>
		public virtual string Lat
		{
			get { return lat; }
			set { lat = value; }
		}

		/// <summary>
		/// Gets or sets the Lon value.
		/// </summary>
		public virtual string Lon
		{
			get { return lon; }
			set { lon = value; }
		}

		#endregion
	}
}
